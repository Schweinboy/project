-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Hostiteľ: localhost
-- Vygenerované: Pi 03.Jan 2014, 16:36
-- Verzia serveru: 5.5.24-log
-- Verzia PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáza: `iiadb4`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `outputs`
--

CREATE TABLE IF NOT EXISTS `outputs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(300) CHARACTER SET utf8 NOT NULL,
  `content` mediumtext CHARACTER SET utf8,
  `language` varchar(2) CHARACTER SET utf8 NOT NULL,
  `username` varchar(300) NOT NULL,
  `link` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `file` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

--test data for
INSERT INTO `outputs`
(`id`,`title`,`content`,`language`,`username`,`link`,`file`)
VALUES
('12', 'Vystup 1', 'Lorem zipsum atd......', 'sk', 'mia', NULL, NULL);

INSERT INTO `outputs`
(`id`,`title`,`content`,`language`,`username`,`link`,`file`)
VALUES
('13', 'Vystup 2', 'Text druheho vystupu', 'sk', 'miriama.niznanska@gmail.com', NULL, NULL);
