-- phpMyAdmin SQL Dump
-- version 4.0.6
-- http://www.phpmyadmin.net
--
-- Hostiteľ: localhost
-- Vygenerované: Pi 03.Jan 2014, 03:17
-- Verzia serveru: 5.1.71
-- Verzia PHP: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáza: `iiadb4`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `STAT_LOGIN`
--

CREATE TABLE IF NOT EXISTS `STAT_LOGIN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_USER` int(5) NOT NULL,
  `DATE` date NOT NULL,
  `TIME` time NOT NULL,
  `IP_USER` varchar(15) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `COUNTRY` varchar(50) NOT NULL,
  `SIGN` varchar(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=167 ;

--
-- Sťahujem dáta pre tabuľku `STAT_LOGIN`
--

INSERT INTO `STAT_LOGIN` (`ID`, `ID_USER`, `DATE`, `TIME`, `IP_USER`, `COUNTRY`, `SIGN`) VALUES
(166, 24, '2014-01-03', '03:59:32', '188.167.240.148', 'Slovakia', 'N'),
(164, 24, '2014-01-02', '21:09:17', '188.167.240.148', 'Slovakia', 'N'),
(165, 1, '2014-01-03', '03:45:12', '188.167.240.148', 'Slovakia', 'N'),
(162, 27, '2014-01-02', '21:07:18', '188.121.167.157', 'Slovakia', 'N'),
(159, 1, '2014-01-02', '20:46:49', '188.167.240.148', 'Slovakia', 'N'),
(160, 25, '2014-01-02', '20:47:17', '188.167.240.148', 'Slovakia', 'N'),
(161, 24, '2014-01-02', '20:47:36', '188.167.240.148', 'Slovakia', 'N');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
