-- phpMyAdmin SQL Dump
-- version 4.0.6
-- http://www.phpmyadmin.net
--
-- Hostiteľ: localhost
-- Vygenerované: Po 30.Dec 2013, 15:46
-- Verzia serveru: 5.1.71
-- Verzia PHP: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáza: `iiadb4`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `STAT_LOGIN`
--

CREATE TABLE IF NOT EXISTS `STAT_LOGIN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_USER` int(5) NOT NULL,
  `DATE` date NOT NULL,
  `TIME` time NOT NULL,
  `IP_USER` varchar(15) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `SIGN` varchar(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Sťahujem dáta pre tabuľku `STAT_LOGIN`
--

INSERT INTO `STAT_LOGIN` (`ID`, `ID_USER`, `DATE`, `TIME`, `IP_USER`, `SIGN`) VALUES
(1, 1, '2013-12-30', '16:28:34', '188.167.240.148', 'N'),
(2, 1, '2013-12-30', '16:29:07', '188.167.240.148', 'N'),
(3, 2, '2013-12-30', '16:29:44', '188.167.240.148', 'N'),
(4, 3, '2013-12-30', '16:30:00', '188.167.240.148', 'N'),
(5, 3, '2013-12-30', '16:30:51', '188.167.240.148', 'N'),
(6, 3, '2013-12-30', '16:31:02', '188.167.240.148', 'N'),
(7, 3, '2013-12-30', '16:32:20', '188.167.240.148', 'N'),
(8, 3, '2013-12-30', '16:42:00', '188.167.240.148', 'N');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `USERS`
--

CREATE TABLE IF NOT EXISTS `USERS` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `surname` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `email` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` varchar(40) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `state` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `login` varchar(15) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `newsletter` varchar(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT 'N',
  `lang_newsletter` varchar(2) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 'sk',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Sťahujem dáta pre tabuľku `USERS`
--

INSERT INTO `USERS` (`ID`, `firstname`, `surname`, `username`, `email`, `password`, `state`, `login`, `newsletter`, `lang_newsletter`) VALUES
(1, 'Miriama ', 'Nižnanská', 'mia', 'miriama.niznanska@gmail.com', 'dlpTOsnz', 'Slovensko', 'own_account', 'N', 'sk'),
(2, 'Miriama', 'Niznanska', 'xniznanska', 'xniznanska@stuba.sk', NULL, 'Slovakia', 'LDAP', 'N', 'sk'),
(3, 'Mia', 'Nižnanská', 'miriama.niznanska@gmail.com', 'miriama.niznanska@gmail.com', NULL, 'Slovakia', 'GOOGLE', 'N', 'sk');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
