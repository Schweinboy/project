<?php

require_once './config.php';
require_once './UserController.php';
require_once './PdfGenerator.php';
require_once './EPubGenerator.php';

class OutputController {
    function __construct() {
        ;
    }
    
    /**
     * Returns output record identified by id
     * @param Integer $id
     * @return DibiRow - output
     */
    public static function getOutput($id) {
        return dibi::query('SELECT * FROM [outputs] WHERE [id] = %i', $id)->fetch();
    }
    
    /**
     * Generates pdf file for given output
     * @param Integer $id - id of output
     * @return PDF file
     */
    public static function generatePdf($id) {
        $outputArr = OutputController::getOutputInfo($id);
        return PdfGenerator::generate($outputArr['author'], $outputArr['title'], $outputArr['text']);
    }
    
    /**
     * 
     * @param type $idString
     */
    public static function generatePdfCombined($idsString) {
        $ids = explode('_', $idsString);
        $outputsMap = array();
        foreach ($ids as $id) {
            $outputsMap[$id] = OutputController::getOutputInfo($id);
        }
        return PdfGenerator::generateForMultiple($outputsMap);
    }
    
    private static function getOutputInfo($id) {
        $toReturn = array();
        $output = OutputController::getOutput($id);
        $toReturn['title'] = $output['title'];
        $toReturn['text'] = $output['content'];
        $toReturn['lang'] = $output['language'];
        $toReturn['link'] = $output['link'];
        $toReturn['file'] = $output['file']; 
        $toReturn['author'] = $output['username'];
        return $toReturn;
    }
    
    /**
     * Generates EPub file for given output
     * @param Integer $id - id of output
     * @return EPub file
     */
    public static function generateEPub($id) {
        $outputArr = OutputController::getOutputInfo($id);
        return EPubGenerator::generate($outputArr['author'], $outputArr['title'], $outputArr['text'], $outputArr['lang']);
    }
    
    /**
     * 
     * @param type $idString
     */
    public static function generateEPubCombined($idsString) {
        $ids = explode('_', $idsString);
        $outputsMap = array();
        foreach ($ids as $id) {
            $outputsMap[$id] = OutputController::getOutputInfo($id);
        }
        return EPubGenerator::generateForMultiple($outputsMap);
    }
}


if(isset($_GET['ids'])) {
    if(isset($_GET['type']) && $_GET['type'] === 'epub') 
        return OutputController::generateEPubCombined($_GET['ids']);
    else
        return OutputController::generatePdfCombined($_GET['ids']);
}