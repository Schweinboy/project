<?php

include 'config.php';
include 'TypeOfUser.php';

session_start();

if (TypeOfUser::isSignUser()) {
    $result = dibi::query('SELECT ID FROM USERS WHERE username=%s', $_SESSION['username']);
    $id = $result->fetchSingle();
    dibi::query('UPDATE STAT_LOGIN SET SIGN=%s WHERE ID_USER = %i', 'N', $id);

    unset($_SESSION['username']);
    unset($_SESSION['user']);
}  else {
    unset($_SESSION['username']);
    unset($_SESSION['user']);
}
header('Location: index.php');
?>