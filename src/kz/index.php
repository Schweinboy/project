<?php
include 'config.php';
require_once('libs/smarty/Smarty.class.php');
include 'TypeOfUser.php';


session_start();

$smarty = new Smarty();
$smarty->setTemplateDir('views');
$smarty->setCompileDir('tmp');
$smarty->setCacheDir('cache');

include 'TypeOfLogin.php';

if (isset($_SESSION['username'])) {
    $result = dibi::query('SELECT * FROM USERS WHERE username=%s', $_SESSION['username']);
    $row = $result->fetch(TRUE);

    if (strcmp($row['login'], 'own_account') == 0) {
        $type_user = 1;
        $smarty->assign('type_user', $type_user);
    }
}

if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
  $language = $_GET['lang'];
  $result = dibi::query('SELECT title, content, language, username, date FROM news WHERE language=%s ORDER BY date DESC', $language);
  $outputs = $result->fetchAll();
  $smarty->assign('outputs', $outputs);
} else {
  $language = 'sk';
  $result = dibi::query('SELECT title, content, language, username, date FROM news WHERE language=%s ORDER BY date DESC', $language);
  $outputs = $result->fetchAll();
  $smarty->assign('outputs', $outputs);
}

$smarty->assign('lang', $lang);
$smarty->assign('type_login', $type_login);
$smarty->assign('activeMenu', 'home');
$smarty->display('default.tpl');
?>
