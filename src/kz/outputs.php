<?php
include 'config.php';
require_once('libs/smarty/Smarty.class.php');

$smarty = new Smarty();
$smarty ->setTemplateDir('views');
$smarty ->setCompileDir('tmp');
$smarty ->setCacheDir('cache');

session_start();

include 'TypeOfUser.php';
include 'TypeOfLogin.php';

$smarty->assign('type_login', $type_login);
$smarty->assign('lang', $lang);
$smarty->assign('activeMenu', 'vystupy');

if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
  $language = $_GET['lang'];
} else {
  $language = 'sk';
}
if (isset($_SESSION['username'])) {
    $result = dibi::query('SELECT * FROM USERS WHERE username=%s', $_SESSION['username']);
    $row = $result->fetch(TRUE);

    if (strcmp($row['login'], 'own_account') == 0) {
        $type_user = 1;
        $smarty->assign('type_user', $type_user);
    }
}

$result = dibi::query('SELECT id, title, content, language, username, link, file FROM outputs WHERE language=%s ORDER BY last_update DESC', $language);
$outputs = $result->fetchAll();
$smarty->assign('outputs', $outputs);

$multiPdfIds = '';
foreach ($outputs as $newsEntry) {
    $multiPdfIds = $multiPdfIds.'_'.$newsEntry['id'];
}
$multiPdfIds = substr($multiPdfIds, 1);

$smarty->assign('multiPdfIds', $multiPdfIds);

$smarty->display('outputs.tpl');
?>