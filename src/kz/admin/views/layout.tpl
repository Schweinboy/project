<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />

        <title>Úvod</title>
        <link rel="stylesheet" type="text/css" href="../libs/bootstrap/css/bootstrap.css" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="../libs/bootstrap/js/bootstrap.js"></script>
        <script src="../libs/ckeditor/ckeditor.js"></script>
        <link type="text/css" href="../libs/bootstrap/css/simple-sidebar.css" rel="stylesheet" media="screen" />
        <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body>
        <div id="wrapper">
            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    <li class="sidebar-brand"></li>
                    <li class="sidebar-brand"></li>
                    <li class="sidebar-brand">
                        {if $type_login == '2'}
                            {$lang['SEKCIA_UZIVATELA']}
                        {elseif $type_login == '1'}
                            {$lang['SEKCIA_ADMINA']}
                        {/if}
                    </li>
                    {if $type_login == '2'}
                        <li><a href="profile.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}">{$lang['PROFIL']}</a></li>
                            {if isset($type_user) && $type_user == 1}
                            <li><a href="new_password.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}">{$lang['NEW_PASSWORD']}</a></li>
                            {/if}
                        <li><a href="newsletter.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}">{$lang['msg9']}</a></li>
                        {elseif $type_login == '1'}
                        <li><a href="users.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}">{$lang['USERS']}</a></li>
                        <li><a href="edit_admin.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}">{$lang['EDIT']}</a></li>
                        <li><a href="registration.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}">{$lang['REGISTRACIA']}</a></li>
                        <li><a href="statistic.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}">{$lang['STATISTIC']}</a></li>
                        <li><a href="addOutput.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}">{$lang['PRIDATVYSTUP']}</a></li>
                        <li><a href="addNews.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}">{$lang['PRIDATNOVNIKU']}</a></li>
                        {/if}
                    <br /><br />
                    <li class="sidebar-brand">{$lang['DALSIE_ODKAZY']}</li>
                    <li><a href="role.php">{$lang['ROZDELENIE_ULOH']}</a></li>
                </ul>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <nav class="navbar navbar-inverse" role="navigation">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="index.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}">Tím 2 koncove zadanie</a>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li {if $activeMenu == 'home'}class="active"{/if}><a href="index.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}">{$lang['UVOD']}</a></li>
                                    <li {if $activeMenu == 'oprojekte'}class="active"{/if}><a href="project.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}">{$lang['OPROJEKTE']}</a></li>
                                    <li {if $activeMenu == 'vystupy'}class="active"{/if}><a href="outputs.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}">{$lang['VYSTUPY']}</a></li>
                                    <li {if $activeMenu == 'sluzby'}class="active"{/if}><a href="services.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}">{$lang['SLUZBY']}</a></li>
                                    <li {if $activeMenu == 'aktuality'}class="active"{/if}><a href="news.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}">{$lang['AKTUALITY']}</a></li>
                                    <li {if $activeMenu == 'kontakt'}class="active"{/if}><a href="contact.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}">{$lang['KONTAKT']}</a></li>
                                </ul>
                                <ul class="nav navbar-nav navbar-right">
                                    {if $type_login == '3'}
                                        <li {if $activeMenu == 'login'}class="active"{/if}><a href="login.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}">{$lang['PRIHLASENIE']}</a></li>
                                {elseif $type_login == '2'}
                                        <li>
                                            <a href="profile.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}" title ="{$lang['PROFIL']}">{$row['firstname']} {$row['surname']} </a></li>
                                        <li>
                                            <a href="logout.php"> {$lang['ODHLASIT']} </a>
                                        </li>
                                    {elseif $type_login == '1'}
                                        <li><a href="index.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}">{$admin_name}</a></li></li>
                                        <li>
                                            <a href="logout.php"> {$lang['ODHLASIT']}</a>
                                        </li>
                                    {/if}
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{$lang['JAZYK']} <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="?lang=sk">Slovečina</a></li>
                                            <li><a href="?lang=en">English</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </nav>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h2>{block name=title}{/block}</h2>
                    {block name=body}{/block}
                </div>
            </div>
        </div>
    </div>

</body>
</html>