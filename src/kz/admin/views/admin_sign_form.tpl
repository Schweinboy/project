{extends file="layout.tpl"}
{block name=title}
    {$lang['PRIHLASENIE_ADMIN']}
{/block}
{block name=body}
    <div class="container">
        <div class="row">
            <div class="span6 offset3">
                <form name="frmUser" method="post" action="index.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}" class="form-horizontal  col-md-3">
                    <h4>{if isset($message)} {$message} {/if}</h4>
                    <div class="control-group">
                        <label class="control-label" for="userName">{$lang['PR_MENO']}</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="userName" placeholder="{$lang['PR_MENO']}" name="username">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="password">{$lang['PASSWORD']}</label>
                        <div class="controls">
                            <input class="form-control" type="password" id="password" placeholder="{$lang['PASSWORD']}" name="password">
                        </div>
                    </div>
                        <br />
                    <input class="btn btn-info offset2" type="submit" name="submit" value="{$lang['PRIHLASENIE']}">  
                </form>
            </div>
        </div>
    </div>
{/block}