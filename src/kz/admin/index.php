<html>  
    <?php
    ini_set('display_errors', 1);
    include '../config.php';
    require_once('../libs/smarty/Smarty.class.php');
    include '../TypeOfUser.php';

    session_start();
    include '../TypeOfLogin.php';
    
    $smarty = new Smarty();
    $smarty->setTemplateDir('views');
    $smarty->setCompileDir('tmp');
    $smarty->setCacheDir('cache');

    $message = '';

    if (count($_POST) > 0) {
        $result = dibi::query('SELECT * FROM ADMIN WHERE username = %s', $_POST['username']);
        $num = $result->getRowCount();

        if ($num == 0) {
            $message = $lang['msg6'];
        } else {
            $row = $result->fetch(TRUE);
            if (strcmp($row['password'], $_POST['password']) == 0) {
                session_start();
                $_SESSION['username'] = $_POST['username'];
                $_SESSION['user'] = 'A';
                if(isset($_GET['lang']) && $_GET['lang'] == 'en'){
                    header('Location: ../index.php?lang=en');
                }else{
                header('Location: ../index.php');
                }
            } else {
                $message = $lang['msg7'];
            }
        }
    }
    
    if (isset($_SESSION['username'])) {
        $result = dibi::query('SELECT * FROM USERS WHERE username=%s', $_SESSION['username']);
        $row = $result->fetch(TRUE);

        if (strcmp($row['login'], 'own_account') == 0) {
            $type_user = 1;
            $smarty->assign('type_user', $type_user);
        }
    }

    $p1 = (isset($_POST['username'])) ? $_POST['username'] : "";

    $smarty->assign('lang', $lang);
    $smarty->assign('type_login', $type_login);
    $smarty->assign('p1', $p1);
    $smarty->assign('message', $message);
    $smarty->assign('activeMenu', 'home');
    $smarty->display('admin_sign_form.tpl');
    ?>

</html>