<?php

include 'config.php';
require_once('libs/smarty/Smarty.class.php');
require './RssUtil.php';
define("UPLOAD_DIR", "files/");

$smarty = new Smarty();
$smarty->setTemplateDir('views');
$smarty->setCompileDir('tmp');
$smarty->setCacheDir('cache');

session_start();

include 'TypeOfUser.php';
include 'TypeOfLogin.php';

$smarty->assign('type_login', $type_login);
$smarty->assign('lang', $lang);
$smarty->assign('activeMenu', '');

if (count($_POST) > 0) {
    if (is_uploaded_file($_FILES["file"]['tmp_name'])) {

        $myFile = $_FILES["file"];

        $name = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);

        // don't overwrite an existing file
        $i = 0;
        $parts = pathinfo($name);
        while (file_exists(UPLOAD_DIR . $name)) {
            $i++;
            $name = $parts["filename"] . "-" . $i . "." . $parts["extension"];
        }

        // preserve file from temporary directory
        $success = move_uploaded_file($myFile["tmp_name"], UPLOAD_DIR . $name);
        if (!$success) {
            echo "<p>Unable to save file.</p>";
        }

        // set proper permissions on the new file
        chmod(UPLOAD_DIR . $name, 0644);

        $vaules = array(
            'title' => $_POST['title'],
            'language' => $_POST['language'],
            'content' => $_POST['content'],
            'link' => $_POST['link'],
            'username' => $_SESSION['username'],
            'file' => $name
        );
    } else {
        $vaules = array(
            'title' => $_POST['title'],
            'language' => $_POST['language'],
            'content' => $_POST['content'],
            'link' => $_POST['link'],
            'username' => $_SESSION['username'],
            'file' => NULL
        );
    }

    $result = dibi::query('INSERT INTO outputs', $vaules);
    RssUtil::generateFeedFile("outputs");
    RssUtil::generateFeedFile("all");
    if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
        header('Location: outputs.php?lang=en');
    } else {
        header('Location: outputs.php');
    }
}

if (TypeOfUser::isAdmin()) {
    $smarty->display('addOutput.tpl');
} else {
    $smarty->display('403.tpl');
}
?>
