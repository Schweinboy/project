<?php

class PdfGenerator {
    
    /**
     * Generates pdf from given data
     * @param String $author
     * @param String $title
     * @param String $text
     */
    public static function generate($author, $title, $text) {
        require_once('./libs/tcpdf/tcpdf.php');

        $pdf = PdfGenerator::preparePdf();
        $pdf->SetAuthor($author);
        $pdf->SetTitle($title);
        
        $pdf = PdfGenerator::generateChapters($pdf, array(array('author' => $author, 'title' => $title, 'text' => $text)));
        
        $pdf->Output($title . '.pdf', 'I');
    }
    
    public static function generateForMultiple($chapters) {
        if(count($chapters) === 0) return;
        
        require_once('./libs/tcpdf/tcpdf.php');
        
        $author = '';
        $title = '';
        
        foreach ($chapters as $ch) {
            $author = $author.', '.$ch['author'];
            $title = $title.', '.$ch['title'];
        }
        
        $author = substr($author, 2);
        $title = substr($title, 2);

        $pdf = PdfGenerator::preparePdf();
        $pdf->SetAuthor($author);
        $pdf->SetTitle($title);
        $pdf->SetFont('freesans');
        
        $pdf = PdfGenerator::generateChapters($pdf, $chapters);
        
        $pdf->Output($title . '.pdf', 'I');
    }
    
    private static function preparePdf() {
        $pdf = new TCPDF('P', 'mm', PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
//        $pdf->SetHeaderData(null, null, $pdfHeaderTitle, $pdfHeaderText, 'PDF_HEADER_STRING');
//        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }
        return $pdf;
    }
    
    private static function generateChapters($pdf, $chapterMap) {
        require './config.php';
        
        $dateString = date_format(new DateTime(), 'd.m.Y H:i:s');
        $first = true;
        
        foreach ($chapterMap as $chapter) {
            $pdf->AddPage();
            if($first === true) {
                $pdf->writeHTML('<p><font size="8">'.$lang['GENERATED_AT'].$dateString.'</font></p>');
                $first = false;
            }
            $pdf->writeHTML(PdfGenerator::generateChapterTitle($chapter['title'], $chapter['author']));
            $pdf->writeHTML($chapter['text']);
            
            if(isset($chapter['link'])){
                $pdf->writeHTML("<br/><br/><a href='".$chapter['link']."'>".$chapter['link']."</a>");
            }
        }
        
        return $pdf;
    }
    
    private static function generateChapterTitle($title, $author) {
        $chTitle = "<h1>".$title."</h1>";
        $chTitle .= "<h4>".$author."</h4>";
        $chTitle .= "<br/>";
        return $chTitle;
    }
}

