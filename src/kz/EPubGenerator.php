<?php

class EPubGenerator {


    public static function generateForMultiple($chapters) {
        if(count($chapters) === 0) return;
        
        include_once './libs/epub/EPub.php';
        
        $bookAuthor = '';
        $bookTitle = '';
        $lang = 'sk';
        
        foreach ($chapters as $ch) {
            $bookAuthor = $bookAuthor.', '.$ch['author'];
            $bookTitle = $bookTitle.', '.$ch['title'];
            $lang = $ch['lang'];
        }
        
        $bookAuthor = substr($bookAuthor, 2);
        $bookTitle = substr($bookTitle, 2);
        
        $book = EPubGenerator::prepareEPub($bookTitle, $bookAuthor, $lang);
        
        foreach ($chapters as $ch) {
            $title = $ch['title'];
            $chapter = EPubGenerator::getContentStart($title). "<h1>".htmlentities($title)."</h1>\n"
            . "<h2>".$ch['author']."</h2>\n"
            . "<p>".$ch['text']."</p>\n";
            
            if(isset($ch['link'])){
                $chapter = $chapter. "<a href='".$ch['link']."'>".$ch['link']."</a>";
            }
            
            $chapter = $chapter.EPubGenerator::getBookEnd();
            $book->addChapter($title, $title.".html", $chapter);
        }
        $book->rootLevel();
        $book->finalize();
        $zipData = $book->sendBook(str_replace(', ', '_', $bookTitle));
    }
    
    private static function getContentStart($title) {
        return 
            "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
            . "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n"
            . "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n"
            . "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
            . "<head>"
            . "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n"
            . "<link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\" />\n"
            . "<title>.$title.</title>\n"
            . "</head>\n"
            . "<body>\n";
    }
    
    private static function getBookEnd() {
        return "</body>\n</html>\n";
    }
    
    private static function prepareEPub($bookTitle, $bookAuthor, $lang) {

        error_reporting(E_ALL | E_STRICT);
        ini_set('error_reporting', E_ALL | E_STRICT);
        ini_set('display_errors', 1);
        $book = new EPub();

        // Title and Identifier are mandatory!
        $book->setTitle($bookTitle);
        $book->setIdentifier("http://JohnJaneDoePublications.com/books/TestBook.html", EPub::IDENTIFIER_URI); // Could also be the ISBN number, prefered for published books, or a UUID.
        $book->setLanguage($lang); // Not needed, but included for the example, Language is mandatory, but EPub defaults to "en". Use RFC3066 Language codes, such as "en", "da", "fr" etc.
        //$book->setDescription("This is a brief description\nA test ePub book as an example of building a book in PHP");
        $book->setAuthor($bookAuthor, $bookAuthor);
        //$book->setPublisher("John and Jane Doe Publications", "http://JohnJaneDoePublications.com/"); // I hope this is a non existant address :)
        //$book->setDate(time()); // Strictly not needed as the book date defaults to time().
        //$book->setRights("Copyright and licence information specific for the book."); // As this is generated, this _could_ contain the name or licence information of the user who purchased the book, if needed. If this is used that way, the identifier must also be made unique for the book.
        //$book->setSourceURL("http://JohnJaneDoePublications.com/books/TestBook.html");
        $book->addDublinCoreMetadata(DublinCore::CONTRIBUTOR, "PHP");

        $cssData = "body {\n  margin-left: .5em;\n  margin-right: .5em;\n  text-align: justify;\n}\n\np {\n  font-family: serif;\n  font-size: 10pt;\n  text-align: justify;\n  text-indent: 1em;\n  margin-top: 0px;\n  margin-bottom: 1ex;\n}\n\nh1, h2 {\n  font-family: sans-serif;\n  font-style: italic;\n  text-align: center;\n  background-color: #6b879c;\n  color: white;\n  width: 100%;\n}\n\nh1 {\n    margin-bottom: 2px;\n}\n\nh2 {\n    margin-top: -2px;\n    margin-bottom: 2px;\n}\n";
        $book->addCSSFile("styles.css", "css1", $cssData);
        return $book;
    }
    
    /**
     * Generates EPub from given data
     * @param String $author
     * @param String $title
     * @param String $text
     */
    public static function generate($author, $title, $text, $lang) {
        include_once './libs/epub/EPub.php';
        
        $book = EPubGenerator::prepareEPub($title, $author, $lang);
        
        $chapter = EPubGenerator::getContentStart($title). "<h1>".$title."</h1>\n"
            . "<h2>".$author."</h2>\n"
            . "<p>".$text."</p>\n"
            . EPubGenerator::getBookEnd();

        $book->addChapter($title, $title.".html", $chapter);
        $book->rootLevel();
        $book->finalize();
        $zipData = $book->sendBook($title);
    }
}