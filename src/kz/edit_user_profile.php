<?php

include 'config.php';

session_start();

if (!empty($_POST['firstname']) && !empty($_POST['surname']) && !empty($_POST['state'])) {
    if ($_GET['t'] == 1) {
        if (!empty($_POST['email'])) {

            $arr = array(
                'firstname' => $_POST['firstname'],
                'surname' => $_POST['surname'],
                'email' => $_POST['email'],
                'state' => $_POST['state']
            );
            dibi::query('UPDATE USERS SET', $arr, 'WHERE username=%s', $_SESSION['username']);
        } else {
            if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
                header('Location: profile.php?m&amp;lang=en');
            } else {
                header('Location: profile.php?m');
            }
        }
    } else {
        $arr = array(
            'firstname' => $_POST['firstname'],
            'surname' => $_POST['surname'],
            'state' => $_POST['state']
        );
        dibi::query('UPDATE USERS SET', $arr, 'WHERE username=%s', $_SESSION['username']);
    }
} else {
    if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
        header('Location: profile.php?m&amp;lang=en');
    } else {
        header('Location: profile.php?m');
    }
}
if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
    header('Location: profile.php?lang=en');
} else {
    header('Location: profile.php');
}
?>