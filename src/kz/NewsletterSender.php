<?php

/**
 * Utility for sending newsletter
 *
 */
class NewsletterSender {
    
    /**
     * 
     * @param array $news - single instance of added news entry 
     * @param type $lang
     */
    public static function send($news) {
        require './config.php';
        
        $lang = $news['language'];
        $sender = "newsletter@iia_tim2.stuba.sk";
        $senderName = "Newsletter";
        $msg = NewsletterSender::generateMessage($news, $lang);
        $subject = "Newsletter";
        
        $header  = 'MIME-Version: 1.0' . "\r\n";
        $header .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
        $header .= "From: ". $senderName . " <" . $sender . ">\r\n";

        $users = dibi::query("SELECT * FROM [USERS] WHERE newsletter = 'Y' AND lang_newsletter = %s", $lang)->fetchAll();
        
        foreach ($users as $user) {
            mail($user['email'], $subject, $msg, $header);
        }
    }
    
    private static function generateMessage($news, $lang) {
        require './config.php';
        
        $maxLength = 250;
        $msg = '';
        $text = $news['content'];
        $rootUrl = $__DIR__ . $__KZ__.'news_d.php?id=';
        
        
        if ($lang == 'en') {
            include 'lang/en.php';
        } else {
            include 'lang/sk.php';
        }
        if(strlen($text) > $maxLength) {
            $text = substr($text, 0, $maxLength).'...';
        }
        
        $msg = '<html><head><title>'.$lang['NEWSLETTER_TITLE'].'</title></head><body>';
        $msg .= '<div>';
        $msg .= '<a href="'.$rootUrl.$news['ID'].'">'.$news['title'].'</a>';
        $msg .= '<br/>';
        $msg .= '<p>'.$text.'</p>';
        $msg .= '</div>';
            
        $msg .= '</body></html>';
        
        return $msg;
    }
}