<?php
include 'config.php';
require_once('libs/smarty/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir('views');
$smarty->setCompileDir('tmp');
$smarty->setCacheDir('cache');

session_start();
include 'TypeOfUser.php';
include 'TypeOfLogin.php';

$p1 = (isset($_POST['firstname'])) ? $_POST['firstname'] : "";
$p2 = (isset($_POST['surname'])) ? $_POST['surname'] : "";
$p3 = (isset($_POST['email'])) ? $_POST['email'] : "";
$p4 = (isset($_POST['state'])) ? $_POST['state'] : '';

$msg1 = '';
$msg2 = '';
if (isset($msg) && $msg == 1) {
    $msg1 = $lang['msg1'];
} elseif (isset($msg) && $msg == 2) {
    $msg2 = $lang['POUZ_UM'];
}

$smarty->assign('lang', $lang);
$smarty->assign('p1', $p1);
$smarty->assign('p2', $p2);
$smarty->assign('p3', $p3);
$smarty->assign('p4', $p4);
$smarty->assign('msg1', $msg1);
$smarty->assign('msg2', $msg2);
$smarty->assign('activeMenu', 'home');
$smarty->assign('type_login', $type_login);
$smarty->display('registration_form.tpl');
?>