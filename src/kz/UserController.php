<?php

class UserController {
    public static function getUser($id) {
        return dibi::query('SELECT * FROM [USERS] WHERE [ID] = %i', $id)->fetch();
    }
    public static function getUserByUsername($username) {
        return dibi::query('SELECT * FROM [USERS] WHERE [username] = %s', $username)->fetch();
    }
}

