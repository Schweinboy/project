<?php
ini_set('display_errors', 1);
include 'config.php';
require_once('libs/smarty/Smarty.class.php');
include 'TypeOfUser.php';


session_start();

$smarty = new Smarty();
$smarty->setTemplateDir('views');
$smarty->setCompileDir('tmp');
$smarty->setCacheDir('cache');

include 'TypeOfLogin.php';


$result = dibi::query('SELECT * FROM STAT_LOGIN s JOIN USERS u ON s.ID_USER=u.ID WHERE s.ID_USER=%s', $_GET['id']);
$all = $result->fetchAll();

$smarty->assign('lang', $lang);
$smarty->assign('all', $all);
$smarty->assign('type_login', $type_login);
$smarty->assign('activeMenu', 'home');
$smarty->display('show_user.tpl');


?>
