<?php

ini_set('display_errors', 1);
include 'config.php';
require_once('libs/smarty/Smarty.class.php');
include 'TypeOfUser.php';


session_start();

$smarty = new Smarty();
$smarty->setTemplateDir('views');
$smarty->setCompileDir('tmp');
$smarty->setCacheDir('cache');

include 'TypeOfLogin.php';

if (count($_POST) > 0) {
    if (isset($_GET['stat_log'])){
    $result = dibi::query('SELECT COUNTRY, COUNT(*) AS COUNT FROM STAT_LOGIN WHERE DATE BETWEEN %s AND %s GROUP BY COUNTRY', $_POST['date_from'], $_POST['date_to']);
    $all = $result->fetchAll();
    $smarty->assign('arr', $all);
    }  else {
        $result = dibi::query('SELECT COUNTRY, COUNT(*) AS COUNT FROM STAT_LOGIN s, USERS u WHERE s.ID_USER=u.ID AND u.login=%s GROUP BY COUNTRY', 'own_account');
        $users = $result->fetchAll();
        $smarty->assign('users', $users);
    }
}


$datetime = get_date();

$smarty->assign('lang', $lang);
$smarty->assign('date', $datetime);
$smarty->assign('type_login', $type_login);
$smarty->assign('activeMenu', 'home');
$smarty->display('statistics.tpl');

function get_date() {
    $date = (new \DateTime())->format('Y-m-d H:i:s');
    return explode(" ", $date);
}

?>
