<?php

include 'config.php';

if (!empty($_POST['firstname']) && !empty($_POST['surname']) && !empty($_POST['state']) && !empty($_POST['email'])) {

    $arr = array(
        'firstname' => $_POST['firstname'],
        'surname' => $_POST['surname'],
        'email' => $_POST['email'],
        'state' => $_POST['state']
    );
    dibi::query('UPDATE USERS SET', $arr, 'WHERE ID=%i', $_GET['id']);
}
if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
        header('Location: edit_admin.php?lang=en');
    } else {
        header('Location: edit_admin.php');
    }
?>