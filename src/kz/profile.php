<?php
include 'config.php';
require_once('libs/smarty/Smarty.class.php');
include 'TypeOfUser.php';

session_start();

$smarty = new Smarty();
$smarty->setTemplateDir('views');
$smarty->setCompileDir('tmp');
$smarty->setCacheDir('cache');

include 'TypeOfLogin.php';

$result = dibi::query('SELECT * FROM USERS WHERE username=%s', $_SESSION['username']);
$row = $result->fetch(TRUE);

$t_user = '0'; //zobrazenie profilu prihlaseneho uzivatela

if (isset($_GET['edit'])) {   //editacia profilu
    if (strcmp($row['login'], 'own_account') == 0) {  //pre editaciu profilu je potrebne zistit, ci je uzivatel registrovany alebo na prihlasenie vyuziva ucet STUBA alebo GOOGLE
        $t_user = '1';
    } else {
        $t_user = '2';
    }
}
if (isset($_SESSION['username'])) {
    $result = dibi::query('SELECT * FROM USERS WHERE username=%s', $_SESSION['username']);
    $row = $result->fetch(TRUE);

    if (strcmp($row['login'], 'own_account') == 0) {
        $type_user = 1;
        $smarty->assign('type_user', $type_user);
    }
}

$smarty->assign('lang', $lang);
$smarty->assign('row', $row);
$smarty->assign('activeMenu', 'home');
$smarty->assign('type_login', $type_login);
$smarty->assign('t_user', $t_user);
$smarty->display('edit_profile.tpl');
?>