<?php

class TypeOfUser {

    public static function isAdmin() {
        if (isset($_SESSION['username']) && $_SESSION['user'] == 'A') {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public static function isSignUser() {
        if (isset($_SESSION['username']) && $_SESSION['user'] == 'U') {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}

?>