<?php
include 'config.php';
require_once('libs/smarty/Smarty.class.php');
include 'TypeOfUser.php';


session_start();

$smarty = new Smarty();
$smarty->setTemplateDir('views');
$smarty->setCompileDir('tmp');
$smarty->setCacheDir('cache');

include 'TypeOfLogin.php';

$result = dibi::query('SELECT * FROM LAST_LOGIN l JOIN USERS u ON l.ID_USER=u.ID');
$all = $result->fetchAll();

$smarty->assign('lang', $lang);
$smarty->assign('all', $all);
$smarty->assign('type_login', $type_login);
$smarty->assign('activeMenu', 'home');
$smarty->display('users.tpl');
?>
