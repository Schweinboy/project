<?php
include 'config.php';

if(isset($_GET['id'])){
    dibi::query('DELETE FROM USERS WHERE ID=%i', $_GET['id']);
    dibi::query('DELETE FROM STAT_LOGIN WHERE ID_USER=%i', $_GET['id']);
    dibi::query('DELETE FROM LAST_LOGIN WHERE ID_USER=%i', $_GET['id']);
}
if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
        header('Location: edit_admin.php?lang=en');
    } else {
        header('Location: edit_admin.php');
    }

?>