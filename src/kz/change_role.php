<?php
ini_set('display_errors', 1);
include 'config.php';
require_once('libs/smarty/Smarty.class.php');
include 'TypeOfUser.php';


session_start();

$smarty = new Smarty();
$smarty->setTemplateDir('views');
$smarty->setCompileDir('tmp');
$smarty->setCacheDir('cache');

include 'TypeOfLogin.php';

$result = dibi::query('SELECT ID, firstname, surname, username FROM USERS WHERE login="own_account"');
$all = $result->fetchAll();

if(isset($_GET['id'])){
    $result = dibi::query('SELECT username, password FROM USERS WHERE ID=%s', $_GET['id']);
    $user = $result->fetchAll();
    foreach ($user as $value) {
        foreach ($value as $key => $v) {
            if(strcmp($key, 'username') == 0){
                $username = $v;
            }  else {
                $pass = $v;
            }            
        }        
    }
    $res = dibi::query('SELECT username FROM ADMIN WHERE username=%s', $username);
    $num_admin = $res->getRowCount();

    if($num_admin == 0){
    $u = array (
        'username' => $username,
        'password' => $pass
    );
    dibi::query('INSERT INTO ADMIN', $u);
    }
}
if(isset($_GET['id_admin'])){
    dibi::query('DELETE FROM ADMIN WHERE ID=%i', $_GET['id_admin']);
}

$result = dibi::query('SELECT ID, username FROM ADMIN');
$arr = $result->fetchAll();

if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
    $l = 'en';
} else {
    $l = '';
}

$smarty->assign('lang', $lang);
$smarty->assign('all', $all);
$smarty->assign('arr', $arr);
$smarty->assign('l', $l);
$smarty->assign('type_login', $type_login);
$smarty->assign('activeMenu', 'home');
$smarty->display('change_role.tpl');
?>
