<?php
if(TypeOfUser::isSignUser()){
    $result = dibi::query('SELECT * FROM USERS WHERE USERNAME = %s', $_SESSION['username']);
    $row = $result->fetch(TRUE);
    $type_login = '2';
    $smarty->assign('row', $row);
}elseif (TypeOfUser::isAdmin()) {
    $admin_name = $_SESSION['username'];
    $type_login = '1';
    $smarty->assign('admin_name', $admin_name);
    $smarty->assign('__ADMIN__', $__ADMIN__);
}  else {
    $type_login = '3';
}
?>