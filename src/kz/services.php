<?php
include 'config.php';
require_once('libs/smarty/Smarty.class.php');

$smarty = new Smarty();
$smarty ->setTemplateDir('views');
$smarty ->setCompileDir('tmp');
$smarty ->setCacheDir('cache');

session_start();

include 'TypeOfUser.php';
include 'TypeOfLogin.php';

if (isset($_SESSION['username'])) {
    $result = dibi::query('SELECT * FROM USERS WHERE username=%s', $_SESSION['username']);
    $row = $result->fetch(TRUE);

    if (strcmp($row['login'], 'own_account') == 0) {
        $type_user = 1;
        $smarty->assign('type_user', $type_user);
    }
}

$smarty->assign('type_login', $type_login);
$smarty->assign('lang', $lang);
$smarty->assign('activeMenu', 'sluzby');
$smarty->display('services.tpl');

?>
