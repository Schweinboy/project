<?php

include 'config.php';
require_once('libs/smarty/Smarty.class.php');
require './RssUtil.php';
define("UPLOAD_DIR", "files/");

$smarty = new Smarty();
$smarty->setTemplateDir('views');
$smarty->setCompileDir('tmp');
$smarty->setCacheDir('cache');

session_start();

include 'TypeOfUser.php';
include 'TypeOfLogin.php';
$smarty->assign('type_login', $type_login);
$smarty->assign('lang', $lang);
$smarty->assign('activeMenu', '');


if (TypeOfUser::isAdmin()) {

    if (isset($_GET['id'])) {
        $result = dibi::query('SELECT file FROM outputs WHERE ID=%i', $_GET['id']);
        $row = $result->fetch(TRUE);

        if ($row['file'] != NULL) {
            if (file_exists(UPLOAD_DIR . $row['file'])) {
                unlink(UPLOAD_DIR . $row['file']);
            }
        }

        $result = dibi::query('DELETE FROM outputs WHERE ID=%i', $_GET['id']);
    }
    if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
        header('Location: outputs.php?lang=en');
    } else {
        header('Location: outputs.php');
    }
} else {
    $smarty->display('403.tpl');
}
?>
