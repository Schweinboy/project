<?php

include 'config.php';
require_once('libs/smarty/Smarty.class.php');
include 'TypeOfUser.php';

$smarty = new Smarty();
$smarty->setTemplateDir('views');
$smarty->setCompileDir('tmp');
$smarty->setCacheDir('cache');

session_start();
include 'TypeOfLogin.php';

if (count($_POST) > 0) {
    $result = dibi::query('SELECT password FROM USERS WHERE username=%s', $_SESSION['username']);
    $pass = $result->fetchSingle();

    if (strcmp($pass, $_POST['old_pass']) == 0) {
        if (!empty($_POST['new_pass1']) && !empty($_POST['new_pass1']) && strcmp($_POST['new_pass1'], $_POST['new_pass2']) == 0) {
            $arr = array(
                'password' => $_POST['new_pass1']
            );
            dibi::query('UPDATE USERS SET', $arr, 'WHERE username=%s', $_SESSION['username']);
            if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
                header('Location: index.php?lang=en');
            } else {
                header('Location: index.php');
            }
        } else {
            $msg = $lang['msg8'];
            $smarty->assign('msg', $msg);
        }
    } else {
        $msg = $lang['msg7'];
        $smarty->assign('msg', $msg);
    }
}
if (isset($_SESSION['username'])) {
    $result = dibi::query('SELECT * FROM USERS WHERE username=%s', $_SESSION['username']);
    $row = $result->fetch(TRUE);

    if (strcmp($row['login'], 'own_account') == 0) {
        $type_user = 1;
        $smarty->assign('type_user', $type_user);
    }
}


$smarty->assign('lang', $lang);
$smarty->assign('type_login', $type_login);
$smarty->assign('activeMenu', 'home');
$smarty->display('new_password.tpl');
?>