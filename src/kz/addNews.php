<?php

include 'config.php';
require_once('libs/smarty/Smarty.class.php');
require './RssUtil.php';
require './NewsletterSender.php';

$smarty = new Smarty();
$smarty->setTemplateDir('views');
$smarty->setCompileDir('tmp');
$smarty->setCacheDir('cache');

session_start();

include 'TypeOfUser.php';
include 'TypeOfLogin.php';

$smarty->assign('type_login', $type_login);
$smarty->assign('lang', $lang);
$smarty->assign('activeMenu', '');

if (count($_POST) > 0) {
    $vaules = array(
        'title' => $_POST['title'],
        'language' => $_POST['language'],
        'content' => $_POST['content'],
        'date' => $_POST['date'],
        'username' => $_SESSION['username']
    );

    $result = dibi::query('INSERT INTO news', $vaules);
    $vaules['ID'] = dibi::insertId();
    RssUtil::generateFeedFile("news");
    RssUtil::generateFeedFile("all");
    NewsletterSender::send($vaules);

    if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
        header('Location: news.php?lang=en');
    } else {
        header('Location: news.php');
    }
}
if (TypeOfUser::isAdmin()) {
    $smarty->display('addNews.tpl');
} else {
    $smarty->display('403.tpl');
}
?>
