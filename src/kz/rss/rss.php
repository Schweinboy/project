<?php

if(isset($_GET['ch'])) {
    $channel = $_GET['ch'];
    if($channel === 'outputs') {
        header('Location: outputs.xml');
    } else if($channel === 'news') {
        header('Location: news.xml');
    } else if($channel === 'all') {
        header('Location: all.xml');
    }
}