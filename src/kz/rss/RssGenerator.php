<?php

/**
 * all methods must be called from script located in root directory
 */
class RssGenerator {
    public static function generateFeedFileAsync($feedName) {
        RssGenerator::generateFeedFile($feedName);
//        $w = new RssWorker($feedName);
//        $w->run();
    }
    public static function generateFeedFile($feedName) {
        require './config.php';
        
        $rootUrl = $__DIR__ . $__KZ__;
        $items = '';
        $rssTitle = '';
        $rssDescription = '';

        switch ($feedName) {
            case 'outputs':
                $items = RssGenerator::generateOutputsItems($rootUrl);
                $rssTitle = $lang['VYSTUPY'];
                $rssDescription = $lang['RSS_OUTPUTS_DESCRIPTION'];
                break;
            case 'news':
                $items = RssGenerator::generateNewsItems($rootUrl);
                $rssTitle = $lang['AKTUALITY'];
                $rssDescription = $lang['RSS_NEWS_DESCRIPTION'];
                break;
            case 'all':
                $items = RssGenerator::generateAllItems($rootUrl);
                $rssTitle = $lang['RSS_ALL_TITLE'];
                $rssDescription = $lang['RSS_ALL_DESCRIPTION'];
                break;
            default:
                break;
        }
        
        $rssfeed = '<?xml version="1.0" encoding="UTF-8"?>';
        $rssfeed .= '<rss version="2.0">';
        $rssfeed .= RssGenerator::generateChannel($rssTitle, $rootUrl, $rssDescription, $items);
        $rssfeed .= '</rss>';
        
        $filename = './rss/'.$feedName.".xml";
        unlink($filename);
        file_put_contents($filename, $rssfeed, LOCK_EX);
    }
    
    private static function generateChannel($rssTitle, $rootUrl, $rssDescription, $items) {
        $channel = '<channel>';
        $channel .= '<title>'.$rssTitle.'</title>';
        $channel .= '<link>'.$rootUrl.'</link>';
        $channel .= '<description>'.$rssDescription.'</description>';
        $channel .= '<language>en-us</language>';
        $channel .= $items;
        $channel .= '</channel>';
        return $channel;
    }
    
    private static function generateAllItems($rootUrl) {
        $items = '';
        $items .= RssGenerator::generateOutputsItems($rootUrl);
        $items .= RssGenerator::generateNewsItems($rootUrl);
        return $items;
    }
    
    private static function generateOutputsItems($rootUrl) {
        require './config.php';
        $result = dibi::query('SELECT * FROM [outputs] WHERE DATEDIFF(CURRENT_TIMESTAMP, last_update) < '.$RSS_TIME.' ORDER BY last_update DESC')->fetchAll();
        $items = '';
        
        foreach ($result as $n => $row) {
            $text = $row['content'];
            $text = substr($text, 0, min(array(200, strlen($text))));
            $items .= '<item>';
            $items .= '<title><![CDATA[' . html_entity_decode($row['title'], ENT_COMPAT, "UTF-8") . ']]></title>';
            $items .= '<description><![CDATA[' . html_entity_decode($text, ENT_COMPAT, "UTF-8") . ']]></description>';
            $items .= '<link>' . $rootUrl . 'output.php?id='. $row['id'] . '</link>';
            $items .= '<pubDate>' . date("D, d M Y H:i:s O", strtotime($row['last_update'])) . '</pubDate>';
            $items .= '</item>';
        }
        return $items;
    }
    
    private static function generateNewsItems($rootUrl) {
        require './config.php';
        $result = dibi::query('SELECT * FROM [news] WHERE DATEDIFF(CURRENT_TIMESTAMP, last_update) < '.$RSS_TIME.' ORDER BY last_update DESC')->fetchAll();
        $items = '';
        
        foreach ($result as $n => $row) {
            $text = $row['content'];
            $text = substr($text, 0, min(array(200, strlen($text))));
            $items .= '<item>';
            $items .= '<title><![CDATA[' . html_entity_decode($row['title'], ENT_COMPAT, "UTF-8") . ']]></title>';
            $items .= '<description><![CDATA[' . html_entity_decode($text, ENT_COMPAT, "UTF-8") . ']]></description>';
            $items .= '<link>' . $rootUrl . 'news_d.php?id='. $row['ID'] . '</link>';
            $items .= '<pubDate>' . date("D, d M Y H:i:s O", strtotime($row['last_update'])) . '</pubDate>';
            $items .= '</item>';
        }
        return $items;
    }
}
