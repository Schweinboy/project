<?php

require_once './RssGenerator.php';

class RssWorker extends Worker {
    public function __construct(&$feedName) {
        $this->feedName = $feedName;
    }
    public function run(){
        RssGenerator::generateFeedFile($this->feedName);
    }
}
