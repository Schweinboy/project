<?php

include 'config.php';
require_once('libs/smarty/Smarty.class.php');
require './RssUtil.php';

$smarty = new Smarty();
$smarty->setTemplateDir('views');
$smarty->setCompileDir('tmp');
$smarty->setCacheDir('cache');

session_start();

include 'TypeOfUser.php';
include 'TypeOfLogin.php';

$smarty->assign('type_login', $type_login);
$smarty->assign('lang', $lang);
$smarty->assign('activeMenu', 'aktuality');
if (TypeOfUser::isAdmin()) {
    if (isset($_GET['id'])) {
        $result = dibi::query('SELECT * FROM news WHERE ID=%i', $_GET['id']);
        $row = $result->fetch(TRUE);
        $smarty->assign('row', $row);

        if (count($_POST) > 0) {
            $vaules = array(
                'title' => $_POST['title'],
                'language' => $_POST['language'],
                'content' => $_POST['content'],
                'date' => $_POST['date'],
                'username' => $_SESSION['username']
            );

            $result = dibi::query('UPDATE news SET ', $vaules, 'WHERE id=%i', $_GET['id']);
            RssUtil::generateFeedFile("news");
            RssUtil::generateFeedFile("all");

            if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
                header('Location: news.php?lang=en');
            } else {
                header('Location: news.php');
            }
        }
    }
    $smarty->display('editNews.tpl');
} else {
    $smarty->display('403.tpl');
}
?>
