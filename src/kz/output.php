<?php
include 'config.php';
require_once('libs/smarty/Smarty.class.php');

$smarty = new Smarty();
$smarty ->setTemplateDir('views');
$smarty ->setCompileDir('tmp');
$smarty ->setCacheDir('cache');

session_start();

include 'TypeOfUser.php';
include 'TypeOfLogin.php';

$smarty->assign('type_login', $type_login);
$smarty->assign('lang', $lang);
$smarty->assign('activeMenu', 'vystupy');

if (isset($_GET['id'])) {
  $id = $_GET['id'];
} else {
    header('Location: outputs.php');
    return;
}

$result = dibi::query('SELECT id, title, content, language, username, link, file FROM outputs WHERE id=%i', $id);
$output = $result->fetch();
$smarty->assign('output', $output);

$smarty->display('output.tpl');
?>