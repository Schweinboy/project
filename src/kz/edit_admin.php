<?php
ini_set('display_errors', 1);
include 'config.php';
require_once('libs/smarty/Smarty.class.php');
include 'TypeOfUser.php';


session_start();

$smarty = new Smarty();
$smarty->setTemplateDir('views');
$smarty->setCompileDir('tmp');
$smarty->setCacheDir('cache');

include 'TypeOfLogin.php';

if(isset($_GET['id'])){
    $result = dibi::query('SELECT * FROM USERS WHERE ID=%i', $_GET['id']);
    $all = $result->fetch(TRUE);
    $edit = 1;
}  else {
    $result = dibi::query('SELECT ID, firstname, surname, username FROM USERS');
    $all = $result->fetchAll();
    $edit = 0;
}

if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
        $l = 'en';
    } else {
        $l = '';
    }

$smarty->assign('lang', $lang);
$smarty->assign('all', $all);
$smarty->assign('edit', $edit);
$smarty->assign('l', $l);
$smarty->assign('type_login', $type_login);
$smarty->assign('activeMenu', 'home');
$smarty->display('edit_admin.tpl');
?>
