<?php
include 'config.php';
require_once('libs/smarty/Smarty.class.php');
require './RssUtil.php';
define("UPLOAD_DIR", "files/");

$smarty = new Smarty();
$smarty->setTemplateDir('views');
$smarty->setCompileDir('tmp');
$smarty->setCacheDir('cache');

session_start();

include 'TypeOfUser.php';
include 'TypeOfLogin.php';

$smarty->assign('type_login', $type_login);
$smarty->assign('lang', $lang);
$smarty->assign('activeMenu', 'vystupy');
if (TypeOfUser::isAdmin()) {

    if (isset($_GET['id'])) {
        $result = dibi::query('SELECT * FROM outputs WHERE ID=%i', $_GET['id']);
        $row = $result->fetch(TRUE);
        $smarty->assign('row', $row);

        if (count($_POST) > 0) {
            if (is_uploaded_file($_FILES["file"]['tmp_name'])) {

                $myFile = $_FILES["file"];

                $name = preg_replace("/[^A-Z0-9._-]/i", "_", $myFile["name"]);

                if ($row['file'] != NULL) {
                    if (file_exists(UPLOAD_DIR . $row['file'])) {
                        unlink(UPLOAD_DIR . $row['file']);
                    }
                }

                // preserve file from temporary directory
                $success = move_uploaded_file($myFile["tmp_name"], UPLOAD_DIR . $name);
                if (!$success) {
                    echo "<p>Unable to save file.</p>";
                }

                // set proper permissions on the new file
                chmod(UPLOAD_DIR . $name, 0644);

                $vaules = array(
                    'title' => $_POST['title'],
                    'language' => $_POST['language'],
                    'content' => $_POST['content'],
                    'link' => $_POST['link'],
                    'username' => $_SESSION['username'],
                    'file' => $name
                );
            } else {
                $vaules = array(
                    'title' => $_POST['title'],
                    'language' => $_POST['language'],
                    'content' => $_POST['content'],
                    'link' => $_POST['link'],
                    'username' => $_SESSION['username'],
                    'file' => $row['file']
                );
            }

            $result = dibi::query('UPDATE outputs SET ', $vaules, 'WHERE id=%i', $_GET['id']);
            RssUtil::generateFeedFile("outputs");
            RssUtil::generateFeedFile("all");
            if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
                header('Location: outputs.php?lang=en');
            } else {
                header('Location: outputs.php');
            }
        }
    }

    $smarty->display('editOutput.tpl');
} else {
    $smarty->display('403.tpl');
}
?>
