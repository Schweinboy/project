<?php

include 'config.php';
require_once('libs/smarty/Smarty.class.php');
require './RssUtil.php';
define("UPLOAD_DIR", "files/");

$smarty = new Smarty();
$smarty->setTemplateDir('views');
$smarty->setCompileDir('tmp');
$smarty->setCacheDir('cache');

session_start();

include 'TypeOfUser.php';
include 'TypeOfLogin.php';
$smarty->assign('type_login', $type_login);
$smarty->assign('lang', $lang);
$smarty->assign('activeMenu', '');


if (TypeOfUser::isAdmin()) {

    if (isset($_GET['id'])) {
        $result = dibi::query('DELETE FROM news WHERE ID=%i', $_GET['id']);
    }
    if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
        header('Location: news.php?lang=en');
    } else {
        header('Location: news.php');
    }
} else {
    $smarty->display('403.tpl');
}
?>
