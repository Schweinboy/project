<?php

include 'config.php';
require_once('libs/smarty/Smarty.class.php');
include 'TypeOfUser.php';

session_start();

$smarty = new Smarty();
$smarty->setTemplateDir('views');
$smarty->setCompileDir('tmp');
$smarty->setCacheDir('cache');

include 'TypeOfLogin.php';

$result = dibi::query('SELECT newsletter FROM USERS WHERE username=%s', $_SESSION['username']);
$news = $result->fetchSingle();

if ($news == 'Y') {
    $newletter = 0;
} else {
    $newletter = 1;
}


if (count($_POST) > 0) {
    if (isset($_POST['newsletter']) && isset($_POST['lang'])) {
        $arr = array(
            'newsletter' => 'Y',
            'lang_newsletter' => $_POST['lang']
        );
        dibi::query('UPDATE USERS SET', $arr, 'WHERE username=%s', $_SESSION['username']);
    } elseif (isset($_POST['newsletter2'])) {
        $arr = array(
            'newsletter' => 'N'
        );
        dibi::query('UPDATE USERS SET', $arr, 'WHERE username=%s', $_SESSION['username']);
    }
    if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
        header('Location: index.php?lang=en');
    } else {
        header('Location: index.php');
    }
}

if (isset($_SESSION['username'])) {
    $result = dibi::query('SELECT * FROM USERS WHERE username=%s', $_SESSION['username']);
    $row = $result->fetch(TRUE);

    if (strcmp($row['login'], 'own_account') == 0) {
        $type_user = 1;
        $smarty->assign('type_user', $type_user);
    }
}

$smarty->assign('lang', $lang);
$smarty->assign('activeMenu', 'home');
$smarty->assign('news', $newletter);
$smarty->assign('type_login', $type_login);
$smarty->display('newsletter.tpl');
?>