<?php

require './rss/RssGenerator.php';

/*
 * Used for calling generate function of RssGenerator
 */
class RssUtil {
    public static function generateFeedFile($feedName) {
        RssGenerator::generateFeedFile($feedName);
    }
}
