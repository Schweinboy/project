<?php
ini_set('display_errors', 1);
include 'config.php';

// zistovanie, ci uz dane pouzivatelske meno nie je obsadene
$result = dibi::query('SELECT username FROM USERS WHERE username = %s', $_POST['username']);

// ak je prazdne niektore pole formularu, vratime formular na doplnenie uzivatelovi
if (empty($_POST['firstname']) || empty($_POST['surname']) || empty($_POST['email']) ||
        empty($_POST['username']) || empty($_POST['state'])) {
    //echo '<h3>' . $lang['msg1'] . '</h3>';
    $msg = 1;
    include 'registration.php';
} elseif ($result->getRowCount() != NULL) { // ak je pouzivatelske meno pouzite, vrati pouzivatela na formular s hlasenim zadania ineho pouzivatelskeho mena
    //echo '<h3>' . $lang['POUZ_UM'] . '</h3>';
    $msg = 2;
    include 'registration.php';
} else { //ak su vsetky polia vyplnene a uzivatelske meno je dostupne
    $pass = rand_passwd();

    // pole, ktore je vlozene do databazy do tabulky USERS -> pri novej registracii chceme evidovat uzivatela
    $arr = array(
        'firstname' => $_POST['firstname'],
        'surname' => $_POST['surname'],
        'username' => $_POST['username'],
        'email' => $_POST['email'],
        'password' => $pass,
        'state' => $_POST['state'],
        'login' => 'own_account'
    );

    dibi::query('INSERT INTO USERS', $arr);

    // posielanie emailu o prihlasovacich udajoch
    $to = $_POST['email'];
    $subject = $lang['PREDMET'];
    $message = $lang['MAIL'] . " " . $lang['PR_MENO'] . ": " . $_POST['username'] . ", " . $lang['PASSWORD'] . ": " . $pass;
    $from = "projekt@stuba.sk";
    $headers = "From:" . $from;
    mail($to, $subject, $message, $headers);

    //vytvorenie session pre prihlaseneho uzivatela

    session_start();
    if (!isset($_SESSION['username'])) {
        $_SESSION['username'] = $_POST['username'];
        $_SESSION['user'] = 'U';
    }

    $result = dibi::query('SELECT ID FROM USERS WHERE username = %s', $_POST['username']);
    $id = $result->fetchSingle();

    $datetime = get_date();

    $ip = $_SERVER['REMOTE_ADDR'];
    $ip_data = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));

    if ($ip_data && $ip_data->geoplugin_countryName != null) {
        $country = $ip_data->geoplugin_countryName;
    }

    $arr2 = array(
    'ID_USER' => $id,
    'DATE' => $datetime[0],
    'TIME' => $datetime[1],
    'IP_USER' => $_SERVER['REMOTE_ADDR'],
    'COUNTRY' => $country,
    'SIGN' => 'Y'
    );
    dibi::query('INSERT INTO STAT_LOGIN', $arr2);
    set_last_login($id);
    if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
       header('Location: index.php?lang=en');
    } else {
        header('Location: index.php');
    }
}

// funkcia na generovanie nahodneho hesla
function rand_passwd($length = 8, $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') {
    return substr(str_shuffle($chars), 0, $length);
}

function get_date() {
    $date = (new \DateTime())->format('Y-m-d H:i:s');
    return explode(" ", $date);
}

function set_last_login($id) {
    $result = dibi::query('SELECT * FROM LAST_LOGIN WHERE ID_USER=%i', $id);
    $num = count($result);
    $datetime = get_date();

    if ($num == 0) {
        $arr = array(
            'ID_USER' => $id,
            'DATE' => $datetime[0],
            'TIME' => $datetime[1],
            'IP' => $_SERVER['REMOTE_ADDR']
        );
        dibi::query('INSERT INTO LAST_LOGIN', $arr);
    } else {
        $arr = array(
            'DATE' => $datetime[0],
            'TIME' => $datetime[1],
            'IP' => $_SERVER['REMOTE_ADDR']
        );
        dibi::query('UPDATE LAST_LOGIN SET', $arr, 'WHERE ID_USER=%i', $id);
    }
}

?>