{extends file="layout.tpl"}
{block name=title}
    {$lang['AKTUALITY']}
{/block}
{block name=body}
    {section name=outputs loop=$outputs}
        <div class="news">
            <h3><a href="news_d.php?id={$outputs[outputs].ID}">{$outputs[outputs].title}</a></h3>
            {if $type_login == '1'}
                <div class="col-md-offset-10">
                    <a class="btn btn-success btn-xs" href="editNews.php?id={$outputs[outputs].ID}{if $lang['UVOD'] == 'Home'}&amp;lang=en{/if}"><i class="glyphicon glyphicon-edit"></i>  {$lang['EDIT_BTN']}</a>
                    <a class="btn btn-danger btn-xs" href="deleteNews.php?id={$outputs[outputs].ID}{if $lang['UVOD'] == 'Home'}&amp;lang=en{/if}"><i class="glyphicon glyphicon-remove"></i> {$lang['DELETE_BTN']}</a>
                </div>
            {/if}
            <small>{$outputs[outputs].date}</small>
            {$outputs[outputs].content}
            <br/><br/>
            <a href="NewsController.php?ids={$outputs[outputs].ID}"><img src='./img/pdf_icon_small.gif' /></a>
            <a href="NewsController.php?type=epub&ids={$outputs[outputs].ID}"><img src='./img/EPUB_small.jpg' /></a>
        </div>
        <hr>
    {/section}
    {block name=export_links}
        <div>
            <br/>
            {$lang['DL_ALL']} <a href="NewsController.php?ids={$multiPdfIds}"><img src='./img/pdf_icon_small.gif' /></a>
            <a href="NewsController.php?type=epub&ids={$multiPdfIds}"><img src='./img/EPUB_small.jpg' /></a>
        </div>
        <a href="rss/rss.php?ch=news"><img src="img/rss.gif"/></a>
    {/block}
{/block}