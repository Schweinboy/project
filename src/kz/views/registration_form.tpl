{extends file="layout.tpl"}
{block name=body}
    <h2>{$lang['REGISTRACIA']}</h2>
    <h4>{$lang['VYPLNENIE_FORMULARA']}</h4>
    <h4>{$msg1}</h4>
        <form method ="post" action ='new_user.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}'> 
            <table cellpadding="3">	
                <tr>
                    <td><label>{$lang['MENO']}</label></td>
                    <td><input class="form-control" type = "text" name ="firstname" value="{$p1}"/></td>
                    <td></td>
                </tr><tr>
                    <td><label>{$lang['PRIEZVISKO']}</label></td>
                    <td><input class="form-control" type = "text" name ="surname" value="{$p2}"/></td>
                    <td></td>
                </tr><tr>
                    <td><label>E-mail:</label></td>
                    <td><input class="form-control" type = "text" name ="email"  value="{$p3}"/></td>
                    <td></td>
                </tr><tr>
                    <td><label>{$lang['USERNAME']}</label></td>
                    <td><input class="form-control" type = "text" name ="username" value=""/></td>
                    <td>{$msg2}</td>
                </tr><tr>
                    <td><label>{$lang['STAT']}</label></td>
                    <td><input class="form-control" type = "text" name ="state" value="{$p4}"/></td>
                    <td></td>
                </tr>
            </table>
            <p><br /><input type="submit" class="btn btn-info" name="submit" value="{$lang['SAVE']}" /></p>
        </form>
{/block}