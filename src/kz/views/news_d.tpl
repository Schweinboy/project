{extends file="layout.tpl"}
{block name=title}
    {$output.title}
{/block}
{block name=body}
        <div class="news">
            <small>{$output.date}</small>
            {$output.content}
            <br/><br/>
            <a href="NewsController.php?ids={$output.ID}"><img src='./img/pdf_icon_small.gif' /></a>
            <a href="NewsController.php?type=epub&ids={$output.ID}"><img src='./img/EPUB_small.jpg' /></a>
        </div>
{/block}