{extends file="layout.tpl"}
{block name=head}
    <link rel="stylesheet" href="libs/bootstrap/css/bootstrap.css">
    <script src="libs/bootstrap/js/bootstrap.js"></script>
{/block}
{block name=body}
    <div class="container">
        <div class="row">
            <div class="span6 offset3">
                <form name="frmUser" method="post" action="?login{if $lang['UVOD'] == 'Home'}&amp;lang=en{/if}" class="form-horizontal  col-md-3">
                    <h3>{$lang['PRIHLASENIE']}</h3>
                    <div class="control-group">
                        <label class="control-label" for="userName">{$lang['PR_MENO']}</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="userName" placeholder="{$lang['PR_MENO']}" name="username">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="password">{$lang['PASSWORD']}</label>
                        <div class="controls">
                            <input class="form-control" type="password" id="password" placeholder="{$lang['PASSWORD']}" name="password">
                        </div>
                    </div>
                    <p>{$message}</p>
                    <div class="control-group">
                        <label class="control-label" for="password">{$lang['PRIHLASENIE_CEZ']}</label>
                        <div class="controls">
                            <label class="radio">
                                <input type="radio" id="reg" name="log_type" checked value="reg">{$lang['own']}
                            </label>

                            <label class="radio">
                                <input type="radio" id="ldap" name="log_type" value="ldap">LDAP (stuba.sk)
                            </label>

                            <label class="radio">
                                <input type="radio" id="open" name="log_type" value="open">GOOGLE
                            </label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <a href="registration.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}">New user?</a>
                        </div>
                    </div>
                    <input class="btn btn-info offset2" type="submit" name="submit" value="{$lang['POTVRDIT']}">  
                </form>
            </div>
        </div>
    </div>


{/block}