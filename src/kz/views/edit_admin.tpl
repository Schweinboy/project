{extends file="layout.tpl"}
{block name=head}
    <link rel="stylesheet" href="libs/bootstrap/css/bootstrap.css">
    <script src="libs/bootstrap/js/bootstrap.js"></script>
{/block}
{block name=body}
    <div class="container">
        <div class="row">
            <div class="span6 offset3">
                {if $edit == 0}
                <h3> {$lang['USERS']}</h3>
                <table cellpadding='7'>
                    <tr>
                        <th>{$lang['MENO']}</th>
                        <th>{$lang['PRIEZVISKO']}</th>
                        <th>{$lang['USERNAME']}</th>
                        <th></th>
                        <th></th>
                    </tr>
                    {section name=arr loop=$all}
                        <tr>
                            <td>{$all[arr].firstname}</td>
                            <td>{$all[arr].surname}</td>
                            <td>{$all[arr].username}</td>  
                            <td><a href="edit_admin.php?id={$all[arr].ID}{if $lang['UVOD'] == 'Home'}&amp;lang=en{/if}">EDIT</a></td>
                            <td><a href="delete_user.php?id={$all[arr].ID}{if $lang['UVOD'] == 'Home'}&amp;lang=en{/if}">DELETE</a></td>
                        </tr>
                    {/section}
                </table>
                {else}
                    <form name="frmUser" method="post" action="edit_user_admin.php?id={$all['ID']}{if $l == 'en'}&amp;lang=en{/if}" class="form-horizontal">
                      <h3>{$lang['EDIT']} {$lang['UZIVATELA']} {$all['username']}</h3>
                        <table cellpadding="3">	
                            <tr>
                                <td><label>{$lang['MENO']}</label></td>
                                <td><input class="form-control" type = "text" name ="firstname" value="{$all['firstname']}"/></td>
                            </tr><tr>
                                <td><label>{$lang['PRIEZVISKO']}</label></td>
                                <td><input class="form-control" type = "text" name ="surname" value="{$all['surname']}"/></td>
                            </tr><tr>
                                    <td><label>E-mail:</label></td>
                                    <td><input class="form-control" type = "text" name ="email" value="{$all['email']}"/></td>
                                </tr><tr>
                                <td><label>{$lang['STAT']}</label></td>
                                <td><input class="form-control" type = "text" name ="state" value="{$all['state']}"/></td>
                            </tr>
                        </table>
                        <input class="btn btn-info offset2" type="submit" name="submit" value="{$lang['SAVE']}">  
                    </form>
                {/if}
                
            </div>
        </div>
    </div>
{/block}