{extends file="layout.tpl"}
{block name=title}
    {$lang['VYSTUPY']}
{/block}
{block name=body}
    {section name=outputs loop=$outputs}
        <h3><a href="output.php?id={$outputs[outputs].id}">{$outputs[outputs].title}</a></h3>
        {if $type_login == '1'}
            <div class="col-md-offset-10">
                <a class="btn btn-success btn-xs" href="editOutputs.php?id={$outputs[outputs].id}{if $lang['UVOD'] == 'Home'}&amp;lang=en{/if}"><i class="glyphicon glyphicon-edit"></i>  {$lang['EDIT_BTN']}</a>
                <a class="btn btn-danger btn-xs" href="deleteOutput.php?id={$outputs[outputs].id}{if $lang['UVOD'] == 'Home'}&amp;lang=en{/if}"><i class="glyphicon glyphicon-remove"></i> {$lang['DELETE_BTN']}</a>
            </div>
        {/if}
        {$outputs[outputs].content}
        {if isset({$outputs[outputs].link})}
            <br/>
            <a target="_blank" href="{$outputs[outputs].link}">{$outputs[outputs].link}</a>
        {/if}
        {if {$outputs[outputs].file} != NULL}
            <br/>
            <a target="_blank" href="files/{$outputs[outputs].file}"><i class="glyphicon glyphicon-file"></i> {$outputs[outputs].file}</a>
        {/if}
        <br/><br/>
        <a href="OutputController.php?ids={$outputs[outputs].id}"><img src='./img/pdf_icon_small.gif' /></a>
        <a href="OutputController.php?type=epub&ids={$outputs[outputs].id}"><img src='./img/EPUB_small.jpg' /></a>
        <hr>
    {/section}
{block name=export_links}
    <div>
        <br/>
        {$lang['DL_ALL']} <a href="OutputController.php?ids={$multiPdfIds}"><img src='./img/pdf_icon_small.gif' /></a>
        <a href="OutputController.php?type=epub&ids={$multiPdfIds}"><img src='./img/EPUB_small.jpg' /></a>
    </div>
    <a href="rss/rss.php?ch=outputs"><img src="img/rss.gif"/></a>
{/block}
{/block}