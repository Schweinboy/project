{extends file="layout.tpl"}
{block name=title}
   {$output.title}
{/block}
{block name=body}
    {$output.content}
    {if isset({$output.link})}
        <br/>
        <a target="_blank" href="{$output.link}">{$output.link}</a>
    {/if}
    {if {$output.file} != NULL}
        <br>
        <a target="_blank" href="files/{$output.file}"><i class="glyphicon glyphicon-file"></i> {$output.file}</a>
        <hr>
    {/if}
    <br/><br/>
    <a href="OutputController.php?ids={$output.id}"><img src='./img/pdf_icon_small.gif' /></a>
    <a href="OutputController.php?type=epub&ids={$output.id}"><img src='./img/EPUB_small.jpg' /></a>
{/block}