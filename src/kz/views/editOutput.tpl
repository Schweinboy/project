{extends file="layout.tpl"}
{block name=title}
    {$lang['UPRAVVYSTUP']}
{/block}
{block name=body}
    <form role="form" name="addOutput" method="post" class="col-md-6" enctype="multipart/form-data">
        <div class="form-group">
            <label for="title">{$lang['NAZOV']}</label>
            <input type="text" name="title" class="form-control" id="title" value="{$row['title']}" required="required">
        </div>
        <div class="form-group">
            <label for="language">{$lang['JAZYK']}</label>
            <select class="form-control" name="language" id="language" >
                <option {if $row['language'] == 'sk'}selected="selected"{/if} value="sk">sk</option>
                <option {if $row['language'] == 'en'}selected="selected"{/if} value="en">en</option>
            </select>
        </div>
        <div class="form-group">
            <label for="content">{$lang['OBSAH']}</label>
            <textarea name="content" class="form-control" id="content" >{$row['content']}</textarea>
        </div>
        <div class="form-group">
            <label for="link">{$lang['ODKAZ']}</label>
            <input name="link" type="text" class="form-control" id="link" value="{$row['link']}" >
        </div>
        <div class="form-group">
            <label for="file">{$lang['SUBOR']}</label>
            <input name="file" type="file" id="file">
        </div>
        <button type="submit" class="btn btn-info">{$lang['PRIDAT']}</button>
    </form>
    <script>
        CKEDITOR.replace( 'content' );
    </script>
{/block}
