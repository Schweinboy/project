{extends file="layout.tpl"}
{block name=head}
    <link rel="stylesheet" href="libs/bootstrap/css/bootstrap.css">
    <script src="libs/bootstrap/js/bootstrap.js"></script>
{/block}
{block name=body}
    <div class="container">
        <div class="row">
            <div class="span6 offset3">
                <form name="frmUser" method="post" action="new_password.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}" class="form-horizontal">
                    {if isset($msg)} <h4>{$msg}</h4> {/if}
                <table cellpadding="3">
                    <tr>
                       <td><label>{$lang['OLD_PASS']}</label></td>
                       <td><input type = "password" name ="old_pass" value=""/></td>
                    </tr>
                    <tr>
                       <td><label>{$lang['NEW_PASS']}</label></td>
                       <td><input type = "password" name ="new_pass1" value=""/></td>
                    </tr>
                    <tr>
                       <td><label>{$lang['NEW_PASS2']}</label></td>
                       <td><input type = "password" name ="new_pass2" value=""/></td>
                    </tr>
                </table>
                       <input class="btn btn-info offset2" type="submit" name="submit" value="{$lang['SAVE']}"> 
                </form>
                
            </div>
        </div>
    </div>
{/block}