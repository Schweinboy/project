{extends file="layout.tpl"}
{block name=head}
   
    
{/block}
{block name=body}
    <div class="container">
        <div class="row">
            <div class="span6 offset3">
                {if isset($arr)}
                <ul class="nav navbar-nav navbar-right">
                    <div id="map_canvas"></div>
                    </ul>
                {/if}
                <form name="frmUser" method="post" action="statistic.php?stat_log{if $lang['UVOD'] == 'Home'}&amp;lang=en{/if}" class="form-horizontal">
                    <h3>{$lang['STAT_LOG']}</h3>
                    <table cellpadding="5">	
                        <tr>
                            <td>{$lang['DATE']}:</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><label>{$lang['DATE_FROM']}</label></td>
                            <td><input type = "date" name ="date_from" value="2014-01-01"/></td>
                        </tr><tr>
                            <td><label>{$lang['DATE_TO']}</label></td>
                            <td><input type = "date" name ="date_to" value="{$date[0]}"/></td>
                        </tr>
                    </table>
                    <br />
                    <input class="btn btn-info offset2" type="submit" name="submit" value="{$lang['POTVRDIT']}">  
                </form>

                {if isset($arr)}
                    <br /><br />
                    <table cellpadding="5">	
                        <tr>
                            <th>{$lang['COUNT_LOGIN']}</th>
                            <th>{$lang['COUNTRY']}</th>
                        </tr>
                        {section name=arr loop=$arr}
                            <tr>
                                <td>{$arr[arr].COUNT}</td>
                                <td>{$arr[arr].COUNTRY}</td>  
                            </tr>
                        {/section}
                    </table> 
                    
                {/if}
                <form name="frmUser" method="post" action="statistic.php?stat_users{if $lang['UVOD'] == 'Home'}&amp;lang=en{/if}" class="form-horizontal">
                     <br /><br />
                     <h3>{$lang['STAT_REGISER']}</h3>
                    <input class="btn btn-info offset2" type="submit" name="submit" value="{$lang['POTVRDIT']}">
                </form>
                {if isset($users)}
                    <br /><br />
                    <table cellpadding="5">	
                        <tr>
                            <th>{$lang['COUNT_LOGIN']}</th>
                            <th>{$lang['COUNTRY']}</th>
                        </tr>
                        {section name=users loop=$users}
                            <tr>
                                <td>{$users[users].COUNT}</td>
                                <td>{$users[users].COUNTRY}</td>  
                            </tr>
                        {/section}
                    </table> 
                    
                {/if}
            </div>
            
        </div>

    </div>
{/block}