{extends file="layout.tpl"}
{block name=title}
    Online laboratórium pre výučbu predmetov
    automatického riadenia
{/block}
{block name=body}
    Vitajte na stránke projektu tímu 2.


    <h3>{$lang['AKTUALITY']}</h3>
    {section name=outputs loop=$outputs}
        <div class="news">
            <h3>{$outputs[outputs].title}</h3>
            <small>{$outputs[outputs].date}</small>
            {$outputs[outputs].content}
        </div>
    {/section}
 {/block}