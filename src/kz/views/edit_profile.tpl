{extends file="layout.tpl"}
{block name=head}
    <link rel="stylesheet" href="libs/bootstrap/css/bootstrap.css">
    <script src="libs/bootstrap/js/bootstrap.js"></script>
{/block}
{block name=body}
    <div class="container">
        <div class="row">
            <div class="span6 offset3">

                {if $t_user == 1 || $t_user == 2}
                    <form name="frmUser" method="post" action="edit_user_profile.php?t={$type_user}{if $lang['UVOD'] == 'Home'}&amp;lang=en{/if}" class="form-horizontal">
                        <h3>{$lang['EDIT']}</h3>
                        <table cellpadding="3">	
                            <tr>
                                <td><label>{$lang['MENO']}</label></td>
                                <td><input class="form-control" type = "text" name ="firstname" value="{$row['firstname']}"/></td>
                            </tr><tr>
                                <td><label>{$lang['PRIEZVISKO']}</label></td>
                                <td><input class="form-control" type = "text" name ="surname" value="{$row['surname']}"/></td>
                            </tr>
                            {if $t_user == 1}
                                <tr>
                                    <td><label>E-mail:</label></td>
                                    <td><input class="form-control" type = "text" name ="email" value="{$row['email']}"/></td>
                                </tr>
                            {/if}
                            <tr>
                                <td><label>{$lang['STAT']}</label></td>
                                <td><input class="form-control" type = "text" name ="state" value="{$row['state']}"/></td>
                            </tr>
                        </table>
                        <input class="btn btn-info offset2" type="submit" name="submit" value="{$lang['SAVE']}">  
                    </form>
                    
                {else}
                    <form name="frmUser" method="post" action="profile.php?edit{if $lang['UVOD'] == 'Home'}&amp;lang=en{/if}" class="form-horizontal">
                        <h3>{$lang['PROFIL_VIEW']}</h3>
                        <table cellpadding="3">
                            <tr>
                                <td><label>{$lang['MENO']}:</label></td>
                                <td><label>{$row['firstname']}</label></td>
                            </tr>
                            <tr>
                                <td><label>{$lang['PRIEZVISKO']}:</label></td>
                                <td><label>{$row['surname']}</label></td>
                            </tr>
                            <tr>
                                <td><label>{$lang['USERNAME']}:</label></td>
                                <td><label>{$row['username']}</label></td>
                            </tr>
                            <tr>
                                <td><label>E-mail:</label></td>
                                <td><label>{$row['email']}</label></td>
                            </tr>
                            <tr>
                                <td><label>{$lang['STAT']}:</label></td>
                                <td><label>{$row['state']}</label></td>
                            </tr>                       
                        </table>
                        <input class="btn btn-info offset2" type="submit" name="submit" value="{$lang['EDIT']}">  
                    </form>
                {/if}


            </div>
        </div>
    </div>
{/block}