{extends file="layout.tpl"}
{block name=head}
    <link rel="stylesheet" href="libs/bootstrap/css/bootstrap.css">
    <script src="libs/bootstrap/js/bootstrap.js"></script>
{/block}
{block name=body}
    <div class="container">
        <div class="row">
            <div class="span6 offset3">
                <form name="frmUser" method="post" action="newsletter.php{if $lang['UVOD'] == 'Home'}?lang=en{/if}" class="form-horizontal">
                    <h3>{$lang['msg9']}</h3>
                    {if $news == 1}
                        <div>
                            <br />
                            <input type = "checkbox" name ="newsletter" value="newsletter"/>
                            <label>
                                {$lang['msg10']} 
                            </label>
                        </div>
                        <div><label>
                                <br />
                                {$lang['JAZYK']}
                            </label>
                            <select name="lang">
                                <option value="sk">Slovensky</option>
                                <option value="en">English</option>
                            </select>
                        </div>
                    {else}
                         <div>
                            <br />
                            <input type = "checkbox" name ="newsletter2" value="newsletter"/>
                            <label>
                                {$lang['msg11']} 
                            </label>
                        </div>
                                
                    {/if}
                    <br />
                    <input class="btn btn-info offset2" type="submit" name="submit" value="{$lang['SAVE']}"> 
                </form>

            </div>
        </div>
    </div>
{/block}