{extends file="layout.tpl"}
{block name=body}
    <div class="container">
        <div class="row">
            <div class="span6 offset3">
                <h3> {$lang['USERS']}</h3>
                <table cellpadding='7'>
                    <tr>
                        <th>{$lang['MENO']}</th>
                        <th>{$lang['PRIEZVISKO']}</th>
                        <th>{$lang['USERNAME']}</th>
                        <th></th>
                        <th></th>
                    </tr>
                    {section name=arr loop=$all}
                        <tr>
                            <td>{$all[arr].firstname}</td>
                            <td>{$all[arr].surname}</td>
                            <td>{$all[arr].username}</td>  
                            <td><a href="change_role.php?id={$all[arr].ID}{if $lang['UVOD'] == 'Home'}&amp;lang=en{/if}">{$lang['SAVE_ADMIN']}</a></td>
                        </tr>
                    {/section}
                </table>
            </div>
                <div class="span6 offset3">
                <h3> {$lang['ADMIN']}</h3>
                <table cellpadding='7'>
                    <tr>
                        <th>{$lang['USERNAME']}</th>
                        <th></th>
                    </tr>
                    {section name=arr loop=$arr}
                        <tr>
                            <td>{$arr[arr].username}</td>
                            {if $admin_name != $arr[arr].username}
                            <td><a href="change_role.php?id_admin={$arr[arr].ID}{if $lang['UVOD'] == 'Home'}&amp;lang=en{/if}">{$lang['DELETE_ADMIN']}</a></td>
                            {/if}
                        </tr>
                    {/section}
                </table>
                </div>
        </div>
    </div>
{/block}