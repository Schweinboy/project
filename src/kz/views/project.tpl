{extends file="layout.tpl"}
{block name=title}
    {$lang['OPROJEKTE']}
{/block}
{block name=body}
    <br />
    <h4>{$lang['ZAK_UDAJE']}</h4>
    <ul>
        <li>{$lang['TYP_PROJEKTU']}: KEGA</li>
        <li>{$lang['KOD_PROJEKTU']}: 032STU-4/2013</li>
        <li>{$lang['TRV_PROJ']}: 2013 - 2015</li>
    </ul>
        <br />
        <h4>{$lang['CIELE_PROJEKTU']}</h4>
        <p>Cieľom projektu je vytvorenie funkčného online laboratória pre výučbu predmetov automatického riadenia.</p>
        <br />
        <h4>{$lang['CHAR_PROJEKTU']}</h4>
        <p>Projekt vznikol ako koncosemestrálne zadanie predmetu Internetové a intranetové aplikácie Fakulty elektrotechniky a informatiky STU BA</p>
        <br />
        <h4>{$lang['ROZDELENIE_ULOH']}</h4>
        <ul>
            <li>Miriama Nižnanská</li>
            <li>Matej Píša</li>
            <li>Jaroslav Varga</li>
        </ul>
{/block}