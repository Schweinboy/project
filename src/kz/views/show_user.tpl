{extends file="layout.tpl"}
{block name=head}
    <link rel="stylesheet" href="libs/bootstrap/css/bootstrap.css">
    <script src="libs/bootstrap/js/bootstrap.js"></script>
{/block}
{block name=body}
    <div class="container">
        <div class="row">
            <div class="span6 offset3">
                <h3> {$lang['LOGIN_USER']}
                    {section name=arr loop=$all}
                        {if $smarty.section.arr.first}
                        {$all[arr].firstname} {$all[arr].surname}
                        {/if}
                    {/section}
                </h3>
                <table cellpadding='7'>
                    <tr>
                        <th>{$lang['DATE']} & {$lang['TIME']}</th>
                        <th>{$lang['IP']}</th>
                    </tr>
                    {section name=arr loop=$all}
                        <tr>
                             <td>{$all[arr].TIME}</td>
                             <td>{$all[arr].IP_USER}</td>
                        </tr>
                    {/section}
                </table>
            </div>
        </div>
    </div>
{/block}