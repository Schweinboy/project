{extends file="layout.tpl"}
{block name=head}
    <link rel="stylesheet" href="libs/bootstrap/css/bootstrap.css">
    <script src="libs/bootstrap/js/bootstrap.js"></script>
{/block}
{block name=body}
    <div class="container">
        <div class="row">
            <div class="span6 offset3">
                <h3> {$lang['LAST_LOGIN']}</h3>
                <table cellpadding='7'>
                    <tr>
                        <th>{$lang['MENO']}</th>
                        <th>{$lang['PRIEZVISKO']}</th>
                        <th>{$lang['USERNAME']}</th>
                        <th>{$lang['DATE']} & {$lang['TIME']}</th>
                        <th>{$lang['IP']}</th>
                        <th>{$lang['PRIHLASENIE_CEZ']}</th>
                    </tr>
                    {section name=arr loop=$all}
                        <tr>
                            <td>{$all[arr].firstname}</td>
                            <td>{$all[arr].surname}</td>
                            <td><a href="show_user.php?id={$all[arr].ID_USER}{if $lang['UVOD'] == 'Home'}&amp;lang=en{/if}">{$all[arr].username}</a></td>
                            <td>{$all[arr].TIME}</td>
                            <td>{$all[arr].IP}</td>
                            <td>{$all[arr].login}</td>            
                        </tr>
                    {/section}
                </table>
            </div>
        </div>
    </div>
{/block}