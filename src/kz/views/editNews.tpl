{extends file="layout.tpl"}
{block name=title}
    {$lang['UPRAVNOVNIKU']}
{/block}
{block name=body}
    <form role="form" name="addOutput" method="post" class="col-md-6" enctype="multipart/form-data">
        <div class="form-group">
            <label for="title">{$lang['NAZOV']}</label>
            <input type="text" name="title" class="form-control" id="title" value="{$row['title']}" required="required">
        </div>
        <div class="form-group">
            <label for="language">{$lang['JAZYK']}</label>
            <select class="form-control" name="language" id="language" >
                <option {if $row['language'] == 'sk'}selected="selected"{/if} value="sk">sk</option>
                <option {if $row['language'] == 'en'}selected="selected"{/if} value="en">en</option>
            </select>
        </div>
        <div class="form-group">
            <label for="date">{$lang['DATUM']}</label>
            <input type="date" name="date" class="form-control" id="date" value="{$row['date']}" required="required">
        </div>
        <div class="form-group">
            <label for="content">{$lang['OBSAH']}</label>
            <textarea name="content" class="form-control" id="content" >{$row['content']}</textarea>
        </div>
        <button type="submit" class="btn btn-info">{$lang['PRIDAT']}</button>
    </form>
    <script>
        CKEDITOR.replace( 'content' );
    </script>
{/block}
