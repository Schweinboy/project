{extends file="layout.tpl"}
{block name=title}
    {$lang['ROZDELENIE_ULOH']}
{/block}
{block name=body}

    <li>Miriama Nižnanská</li>
    <ul>
        <li>Prihlasovanie užívateľov cez rôzne účty, registrácia užívateľov</li>
        <li>Editácia profilu užívateľa, zmena hesla, odoberanie newsletteru </li>
        <li>Prihlasovane admina, editácia profilu užívateľa, štatistiky o prihlásení, registrácia užívateľa</li>
        <li>O projekte, Kontakt</li>
        <li>Dvojjazyčnosť stránky</li>
    </ul>
    <li>Matej Píša</li>
    <ul>
        <li>Novinky - pridávanie, zmazanie, editovanie, newsletter</li>
        <li>Články - pridávanie, zmazanie, editovanie</li>
        <li>Generovanie iných linkov pre anglické mutácie</li>
        <li>Pridavanie vystupov a stranky 403 s nepomovlenym opravenenim</li>
        <li>Mobilné zobrazenie</li>
    </ul>
    <li>Jaroslav Varga</li>
    <ul>
        <li>Aktuality a články do pdf a epub</li>
        <li>Generovanie PDF do viacerých výstupov do jedného PDF</li>
        <li>Generovanie E-BOOKu podľa aktualít a výstupov</li>
    </ul>

{/block}