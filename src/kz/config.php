<?php

require_once ("libs/dibi/dibi.php");

//relativna cesta 
$__DIR__ = 'http://vmxniznanska.fei.stuba.sk/';
$__KZ__ = 'kz/';
$__ADMIN__ = 'admin/';

//maximalny casovy rozdiel aktualizacie prispevku oproti sucasnosti, 
//na zaklade ktoreho bude/nebude zaradeny do RSS Feedu
$RSS_TIME = 7; //dni

//urcovanie jazyka podla parametra lang v URL
if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
    include 'lang/en.php';
} else {
    include 'lang/sk.php';
}

// urcenie casovej zony
date_default_timezone_set('Europe/Prague');

// pripojenie k databaze
dibi::connect(array(
    'driver' => 'mysql',
    'host' => 'localhost',
    'username' => 'iiauser',
    'password' => 'IIAPASSWORD',
    'database' => 'iiadb4',
    'charset' => 'utf8',
));

// pripojenie k STUBA.sk cez LDAP
$ldaphost = 'ldap.stuba.sk';
$ldapport = 389;
$ldapconn = ldap_connect($ldaphost, $ldapport) or die("Could not connect to $ldaphost");

?>