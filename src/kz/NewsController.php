<?php

require_once './config.php';
require_once './UserController.php';
require_once './PdfGenerator.php';
require_once './EPubGenerator.php';

class NewsController {
    /**
     * Returns News record identified by id
     * @param Integer $id
     * @return DibiRow - output
     */
    public static function getNews($id) {
        return dibi::query('SELECT * FROM [news] WHERE [ID] = %i', $id)->fetch();
    }
    
    /**
     * Generates pdf file for given output
     * @param Integer $id - id of output
     * @return PDF file
     */
    public static function generatePdf($id) {
        $newsArr = NewsController::getNewsInfo($id);
        return PdfGenerator::generate($newsArr['author'], $newsArr['title'], $newsArr['text']);
    }
    
    /**
     * 
     * @param type $idString
     */
    public static function generatePdfCombined($idsString) {
        $ids = explode('_', $idsString);
        $newsMap = array();
        foreach ($ids as $id) {
            $newsMap[$id] = NewsController::getNewsInfo($id);
        }
        return PdfGenerator::generateForMultiple($newsMap);
    }
    
    private static function getNewsInfo($id) {
        $toReturn = array();
        $news = NewsController::getNews($id);
        $toReturn['title'] = $news['title'];
        $toReturn['text'] = $news['content'];
        $toReturn['lang'] = $news['language'];
        $toReturn['author'] = $news['username'];
        return $toReturn;
    }
    
    /**
     * Generates EPub file for given output
     * @param Integer $id - id of output
     * @return EPub file
     */
    public static function generateEPub($id) {
        $newsArr = NewsController::getNewsInfo($id);
        return EPubGenerator::generate($newsArr['author'], $newsArr['title'], $newsArr['text'], $newsArr['lang']);
    }
    
    /**
     * 
     * @param type $idString
     */
    public static function generateEPubCombined($idsString) {
        $ids = explode('_', $idsString);
        $newsMap = array();
        foreach ($ids as $id) {
            $newsMap[$id] = NewsController::getNewsInfo($id);
        }
        return EPubGenerator::generateForMultiple($newsMap);
    }
}

if(isset($_GET['ids'])) {
    if(isset($_GET['type']) && $_GET['type'] === 'epub') 
        return NewsController::generateEPubCombined($_GET['ids']);
    else
        return NewsController::generatePdfCombined($_GET['ids']);
}
