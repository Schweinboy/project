<?php

include 'config.php';
require_once('libs/smarty/Smarty.class.php');

$smarty = new Smarty();
$smarty->setTemplateDir('views');
$smarty->setCompileDir('tmp');
$smarty->setCacheDir('cache');

include 'config.php';
require 'libs/lightopenid/openid.php';
include 'TypeOfUser.php';
include 'TypeOfLogin.php';

$message = "";
if (count($_POST) > 0 || isset($_GET['login'])) {
    if ($_POST['log_type'] == 'reg') {
        $result = dibi::query('SELECT ID, username, password, login FROM USERS WHERE username = %s', $_POST['username']);
        $row = $result->fetch(TRUE);

        $ip = $_SERVER['REMOTE_ADDR'];
        $ip_data = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));

        $country = 'unknown';
        if ($ip_data && $ip_data->geoplugin_countryName != null) {
            $country = $ip_data->geoplugin_countryName;
        }

        if (strcmp($row['password'], $_POST['password']) == 0 && strcmp($row['login'], 'own_account') == 0) {
            $datetime = get_date();
            $arr = array(
                'ID_USER' => $row['ID'],
                'DATE' => $datetime[0],
                'TIME' => $datetime[1],
                'IP_USER' => $_SERVER['REMOTE_ADDR'],
                'COUNTRY' => $country,
                'SIGN' => 'Y'
            );
            dibi::query('INSERT INTO STAT_LOGIN', $arr);
            set_last_login($row['ID']);
            session_start();
            $_SESSION['username'] = $_POST['username'];
            $_SESSION['user'] = 'U';
            if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
                header('Location: index.php?lang=en');
            } else {
                header('Location: index.php');
            }
        } else {
            $message = "<div class='alert'>Invalid Username or Password!</div>";
        }
    } elseif ($_POST['log_type'] == 'ldap') {
        if ($ldapconn) {

            $login = $_POST['username'];
            $pass = $_POST['password'];

            $dn = 'ou=People, DC=stuba, DC=sk';
            $ldaprdn = 'uid=' . $_POST['username'] . ', ou=People, DC=stuba, DC=sk';
            $bind = ldap_bind($ldapconn, $ldaprdn, $pass);

            if ($bind) {

                $result = dibi::query('SELECT * FROM USERS WHERE username = %s', $_POST['username']);
                $num = count($result);

                if ($num == 0) {
                    $ldapFilter = array("givenname", "sn", "mail");
                    $results = ldap_search($ldapconn, $dn, 'uid=xniznanska', $ldapFilter);
                    $info = ldap_get_entries($ldapconn, $results);

                    $ip = $_SERVER['REMOTE_ADDR'];
                    $ip_data = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));

                    if ($ip_data && $ip_data->geoplugin_countryName != null) {
                        $country = $ip_data->geoplugin_countryName;
                    }

                    $arr = array(
                        'firstname' => $info[0]['givenname'][0],
                        'surname' => $info[0]['sn'][0],
                        'username' => $_POST['username'],
                        'email' => $info[0]['mail'][1],
                        'state' => $country,
                        'login' => 'LDAP'
                    );

                    dibi::query('INSERT INTO USERS', $arr);
                    $res = dibi::query('SELECT ID FROM USERS WHERE username = %s', $_POST['username']);
                    $row = $res->fetchAll();
                    $datetime = get_date();
                    $arr2 = array(
                        'ID_USER' => $row[0]['ID'],
                        'DATE' => $datetime[0],
                        'TIME' => $datetime[1],
                        'IP_USER' => $_SERVER['REMOTE_ADDR'],
                        'COUNTRY' => $country,
                        'SIGN' => 'Y'
                    );
                    dibi::query('INSERT INTO STAT_LOGIN', $arr2);
                    set_last_login($row[0]['ID']);
                } else {
                    $row = $result->fetchAll();
                    $datetime = get_date();
                    $ip = $_SERVER['REMOTE_ADDR'];
                    $ip_data = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));

                    if ($ip_data && $ip_data->geoplugin_countryName != null) {
                        $country = $ip_data->geoplugin_countryName;
                    }
                    $arr = array(
                        'ID_USER' => $row[0]['ID'],
                        'DATE' => $datetime[0],
                        'TIME' => $datetime[1],
                        'IP_USER' => $_SERVER['REMOTE_ADDR'],
                        'COUNTRY' => $country,
                        'SIGN' => 'Y'
                    );
                    dibi::query('INSERT INTO STAT_LOGIN', $arr);
                    set_last_login($row[0]['ID']);
                }
                session_start();
                $_SESSION['username'] = $login;
                $_SESSION['user'] = 'U';

                ldap_close($ldapconn);
                if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
                    header('Location: index.php?lang=en');
                } else {
                    header('Location: index.php');
                }
            } else {
                $message = "<div class='alert'>Invalid LDAP Credencials!</div>";
            }
        }
    } else {
        try {
            $openid = new LightOpenID('http://vmxniznanska.fei.stuba.sk/');
            if (!$openid->mode) {

                $openid->identity = 'https://www.google.com/accounts/o8/id';
                $openid->required = array('namePerson/first', 'namePerson/last', 'contact/email');
                header('Location: ' . $openid->authUrl());
            } elseif ($openid->mode == 'cancel') {
                echo 'User has canceled authentication!';
            } else {
                $returnv = $openid->getAttributes();
                $username = $returnv ['contact/email'];
                $name = $returnv ['namePerson/first'];
                $surname = $returnv['namePerson/last'];

                $result = dibi::query('SELECT * FROM USERS WHERE username = %s', $username);
                $num = count($result);
                if ($num == 0) {

                    $ip = $_SERVER['REMOTE_ADDR'];
                    $ip_data = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));

                    if ($ip_data && $ip_data->geoplugin_countryName != null) {
                        $country = $ip_data->geoplugin_countryName;
                    }

                    $arr = array(
                        'firstname' => $name,
                        'surname' => $surname,
                        'username' => $username,
                        'email' => $username,
                        'state' => $country,
                        'login' => 'GOOGLE'
                    );
                    dibi::query('INSERT INTO USERS', $arr);
                    $res = dibi::query('SELECT ID FROM USERS WHERE username = %s', $username);
                    $row = $res->fetchAll();
                    set_last_login($row[0]['ID']);
                    $datetime = get_date();
                    $arr2 = array(
                        'ID_USER' => $row[0]['ID'],
                        'DATE' => $datetime[0],
                        'TIME' => $datetime[1],
                        'IP_USER' => $_SERVER['REMOTE_ADDR'],
                        'COUNTRY' => $country,
                        'SIGN' => 'Y'
                    );
                    dibi::query('INSERT INTO STAT_LOGIN', $arr2);
                } else {
                    $row = $result->fetchAll();
                    $datetime = get_date();
                    set_last_login($row[0]['ID']);
                    $ip = $_SERVER['REMOTE_ADDR'];
                    $ip_data = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));

                    if ($ip_data && $ip_data->geoplugin_countryName != null) {
                        $country = $ip_data->geoplugin_countryName;
                    }
                    $arr = array(
                        'ID_USER' => $row[0]['ID'],
                        'DATE' => $datetime[0],
                        'TIME' => $datetime[1],
                        'IP_USER' => $_SERVER['REMOTE_ADDR'],
                        'COUNTRY' => $country,
                        'SIGN' => 'Y'
                    );
                    dibi::query('INSERT INTO STAT_LOGIN', $arr);
                    session_start();
                    $_SESSION['username'] = $username;
                    $_SESSION['user'] = 'U';

                    if (isset($_GET['lang']) && $_GET['lang'] == 'en') {
                        header('Location: index.php?lang=en');
                    } else {
                        header('Location: index.php');
                    }
                }
            }
        } catch (ErrorException $e) {
            echo $e->getMessage();
        }
    }
}

$smarty->assign('lang', $lang);
$smarty->assign('activeMenu', 'login');
$smarty->assign('type_login', $type_login);
$smarty->assign('message', $message);
$smarty->display('login.tpl');

// funkcia vracia 2-prvkove pole, v prvom prvku je aktualny datum, v druhom prvku aktualny cas (pre potrebu INSERTU v databaze)
function get_date() {
    $date = (new \DateTime())->format('Y-m-d H:i:s');
    return explode(" ", $date);
}

function set_last_login($id) {
    $result = dibi::query('SELECT * FROM LAST_LOGIN WHERE ID_USER=%i', $id);
    $num = count($result);
    $datetime = get_date();

    if ($num == 0) {
        $arr = array(
            'ID_USER' => $id,
            'DATE' => $datetime[0],
            'TIME' => $datetime[1],
            'IP' => $_SERVER['REMOTE_ADDR']
        );
        dibi::query('INSERT INTO LAST_LOGIN', $arr);
    } else {
        $arr = array(
            'DATE' => $datetime[0],
            'TIME' => $datetime[1],
            'IP' => $_SERVER['REMOTE_ADDR']
        );
        dibi::query('UPDATE LAST_LOGIN SET', $arr, 'WHERE ID_USER=%i', $id);
    }
}

?>